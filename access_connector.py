import pyodbc
import pandas as pd
from email_marketing_cdo.config import config
import os

def connect(db_file, user='admin', password = '', old_driver=False):
    driver_ver = '*.mdb'
    if not old_driver:
        driver_ver += ', *.accdb'

    odbc_conn_str = ('DRIVER={Microsoft Access Driver (%s)}'
                     ';DBQ=%s;UID=%s;PWD=%s' %
                     (driver_ver, db_file, user, password))

    return pyodbc.connect(odbc_conn_str)

def query(sql, env=None):
    if env is None:
        env = os.environ.get('python_env')
    conn = connect(config[env]['access_db_file_path'])  # only absolute paths!
    # cursor = conn.cursor
    # cursor.query(sql)
    
    df_res = pd.read_sql(sql,conn)
    conn.close()
    return df_res

if __name__=="__main__":
    df = query(sql="SELECT * FROM Eloqua_contacts")
    print(df.head())