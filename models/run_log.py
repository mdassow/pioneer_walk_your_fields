from mongoengine import connect, Document, StringField, DateTimeField, IntField, EmbeddedDocument, EmbeddedDocumentField, EmbeddedDocumentListField
import os
from datetime import datetime

from email_marketing_cdo.config import config

user = config[os.environ.get('python_env')]['mongo_server_user']
pw = config[os.environ.get('python_env')]['mongo_server_pw']
cluster_url = config[os.environ.get('python_env')]['mongo_cluster_url']
db = config[os.environ.get('python_env')]['mongo_env']
conn_string = 'mongodb+srv://%s:%s@%s/%s?retryWrites=true&w=majority' % (user, pw, cluster_url, db)
connect('prod'
    , host=conn_string
)


class qa_report(EmbeddedDocument):
    input_record_count = IntField(required=True)
    output_record_count = IntField(required=True)

class process_log(EmbeddedDocument):
    data = StringField(required=False)
    error = StringField(required=False)
    backtrace = StringField(required=False)
    created_datetime = DateTimeField(default=datetime.now())
    event_name = StringField(required=True)
    log_level = IntField()


class run_log(Document):
    # _id = ObjectIdField(required=True)
    account = StringField(required=True, max_length=200)
    channel = StringField(required=True, max_length=200)
    client_id = IntField(required=True)
    run_start_datetime = DateTimeField(required=True, default=datetime.now())
    client_name = StringField(required=True, max_length=200)
    endpoint = StringField(required=False, max_length=200)
    qa_report = EmbeddedDocumentField(qa_report, required=False)
    qa_status = IntField(required=False)
    run_end_datetime = DateTimeField(required=False)
    processing_log = EmbeddedDocumentListField(process_log, required=False)

    meta = {'collection': 'elt_run_logs'
            , 'indexes': [
                            {
                                'name': 'expire_date'
                                , 'fields': ['run_start_datetime']
                                , 'expireAfterSeconds': 60 * 60 * 24 * 365
                            }
                            ]}

    def add_process_log(self, **kwargs):
        try:
            p_log = process_log(**kwargs)
            self.processing_log.append(p_log)
            self.save()
        except Exception as e:
            print(e)
            print(kwargs)

if __name__=='__main__':
    # query_set = run_log.objects(id='5cfa865ff78bbc0008886e4f')
    # for log in query_set:
    #     print(log._fields)

    r_log = run_log(account='marketing_cdo'
                    , channel='eloqua'
                    , client_id=29
                    , client_name='Pioneer'
                    )
    r_log.add_process_log(data = None, error = 'Testing', backtrace = None, event_name = 'Testing', log_level = 0)
    r_log.save()