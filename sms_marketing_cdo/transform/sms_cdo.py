import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')

from sms_marketing_cdo.extract import mp_cdo, optin_contacts, version_name_cu, version_name_area, version_name_territory, sms_log_cdo, cu_num

def groupby(data, key_fields):
    def create_dictionary_arrays(d: dict):
        for k, v in d.items():
            d[k] = [v]
        return d

    objectsByKeyValue = {}
    if type(data) is list:
        for i in data:
            key = ''.join([str(i[x]) for x in key_fields])
            try:
                # objectsByKeyValue[key].append(i)
                [objectsByKeyValue[key][k].append(v) for k, v in i.items()]
            except KeyError as ke:
                objectsByKeyValue[key] = create_dictionary_arrays(d=i)
            except Exception as e:
                logger.error(e)
    else:
        raise NotImplementedError
    return objectsByKeyValue

def join() -> list:
    """Requests cdo extract, makes transformations, processes filters and returns data as pandas data frame"""
    try:
        mp_cdo_data = mp_cdo.get_data()
        mp_record_dict = {}
        for mp_record in mp_cdo_data:
            mp_record_dict[mp_record['Email_Address']] = mp_record

        optin_contacts_data = optin_contacts.get_data()
        optin_contacts_dict = {}
        for optin_record in optin_contacts_data:
            optin_contacts_dict[optin_record['Email_Address']] = optin_record

        common_emails = mp_record_dict.keys() & optin_contacts_dict.keys()
        common_email_res = []
        for key in common_emails:
            temp_dict = {}
            temp_dict.update(optin_contacts_dict[key])
            temp_dict.update(mp_record_dict[key])

            # Add hard coded fields:
            temp_dict['CampaignID'] = 'DUPPEB.00000'
            temp_dict['CDOUpdate'] = ''
            temp_dict['DataSource'] = "API_Eloqua_Contacts"
            temp_dict['AudienceID'] = "1"
            temp_dict['MessageID'] = "1"
            temp_dict['SourceID'] = "1"
            temp_dict['Channel'] = "SMS"
            temp_dict['CreativeName'] = "WeeklySMS"
            temp_dict['CreativeID'] = "VA"
            temp_dict['CID'] = "DUPPEB00000"
            temp_dict['Phone'] = "Eloqua_Mobile_Terr"
            temp_dict['Address'] = "MP_CDO"
            temp_dict['MobilePhoneScrubbed'] = optin_contacts_dict[key]['Mobile_Phone']
            temp_dict['VersionNameAllUS'] = "WeeklyAllUSSMSProd"
            temp_dict['Terr_Code'] = mp_record_dict[key]['Territory'][:3]
            common_email_res.append(temp_dict)

            # Check if record passes validations
            if (optin_contacts_dict[key]['Mobile_Phone'] is not None) & \
                    (str(optin_contacts_dict[key]['Mobile_Phone'])[0] != '0') & \
                    (str(optin_contacts_dict[key]['Mobile_Phone'])[0] != '1') & \
                    (len(str(optin_contacts_dict[key]['Mobile_Phone']).strip()) > 0) & \
                    ('lacek' not in mp_record_dict[key]['Email_Address'].lower()) & \
                    (mp_record_dict[key]['Territory'] is not None):
                temp_dict['Suppression'] = None
            else:
                # logger.debug('%s did not pass validation' %(key))
                temp_dict['Suppression'] = 'Does not have valid Mobile_Phone, Email or Territory'

        version_name_cu_data = version_name_cu.query()
        version_name_cu_dict = {}
        for version_name_cu_record in version_name_cu_data:
            version_name_cu_dict[version_name_cu_record['Terr_Code']] = version_name_cu_record

        for common_email_record in common_email_res:
            try:
                common_email_record['VersionNameCU'] = version_name_cu_dict[common_email_record['Terr_Code']]['VersionNameCU']
            except KeyError as ke:
                logger.debug(ke)
            except Exception as e:
                logger.error(e, exc_info=True)

        version_name_area_data = version_name_area.query()
        version_name_area_dict = {}
        for version_name_area_record in version_name_area_data:
            version_name_area_dict[version_name_area_record['Terr_Code']] = version_name_area_record

        for common_email_record in common_email_res:
            try:
                common_email_record['VersionNameArea'] = version_name_area_dict[common_email_record['Terr_Code']]['VersionNameArea']
            except KeyError as ke:
                logger.debug(ke)
            except Exception as e:
                logger.error(e, exc_info=True)


        version_name_territory_data = version_name_territory.query()
        version_name_territory_dict = {}
        for version_name_territory_record in version_name_territory_data:
            version_name_territory_dict[version_name_territory_record['Terr_Code']] = version_name_territory_record

        for common_email_record in common_email_res:
            try:
                common_email_record['VersionNameTerritory'] = version_name_territory_dict[common_email_record['Terr_Code']]['VersionNameTerritory']
            except KeyError as ke:
                if common_email_record['Terr_Code'] not in version_name_territory_dict.keys():
                    common_email_record['VersionNameTerritory'] = '9'
                # logger.debug(ke)
            except Exception as e:
                logger.error(e, exc_info=True)

        # Suppression processing in pandas
        # def verify(row):
        #     for x in row:
        #         if ('stop' in str(v2)[:4].lower()) or ('cancel' in str(v2)[:6].lower()):
        #             return "Opted_Out"
    #         return None
        #
        # sms_log_data = sms_log_cdo.get_data()
        # sms_log_df_grouped = pd.DataFrame(sms_log_data).groupby(['From_Mobile_Phone'])['Content1'].unique()
        # sms_log_df_grouped_suppress = sms_log_df_grouped.apply(verify)

        # Suppress Canada records
        cu_num_data = cu_num.query()
        cu_num_dict = {}
        for cu_num_record in cu_num_data:
            cu_num_dict[cu_num_record['Terr_Code']] = cu_num_record

        for common_email_record in common_email_res:
            try:
                if common_email_record['Terr_Code'] in cu_num_dict.keys():
                    if cu_num_dict[common_email_record['Terr_Code']]['CU_Num'] == 'CU5':
                        common_email_record['VersionNameArea'] = None
                        common_email_record['VersionNameCU'] = None
                        common_email_record['VersionNameAllUS'] = None
                        common_email_record['VersionNameTerritory'] = None
                        common_email_record['Suppression'] = "Canada"
            except KeyError as ke:
                logger.debug(ke)
            except Exception as e:
                logger.error(e, exc_info=True)

        sms_log_data = sms_log_cdo.get_data()
        sms_log_dict = groupby(data=sms_log_data, key_fields=['From_Mobile_Phone'])
        for k, v in sms_log_dict.items():
            v['Suppression'] = None
            for v2 in v['Content1']:
                if ('stop' in str(v2).strip()[:4].lower()) \
                        or ('cancel' in str(v2).strip()[:6].lower())\
                        or ():
                    v['Suppression'] = "Opted_Out"
                    break

        # join sms_log_dict into common_email_record
        for common_email_record in common_email_res:
            try:
                common_email_record.update(sms_log_dict[str(common_email_record['Mobile_Phone'])])
                # common_email_record['sms_log'] = sms_log_dict[str(common_email_record['Mobile_Phone'])]
                # common_email_record['Suppression'] = (sms_log_dict[str(common_email_record['Mobile_Phone'])])['Suppression']
                # print(common_email_record)
            except KeyError as ke:
                # If haven't received SMS Log record set defaults
                common_email_record['sms_log'] = []
                common_email_record['Suppression'] = None
                # logger.debug(ke)
            except Exception as e:
                logger.error(e, exc_info=True)

        # df = pd.DataFrame(common_email_res)
        # df_count = df.groupby('Mobile_Phone').count()
        # df_unique_emails = df.groupby('Mobile_Phone')['Email_Address'].unique()
        # df_unique_territory = df.groupby('Mobile_Phone')['Territory'].unique()
        # mobile_phone_with_multiple_territories = df_unique_territory[df_unique_territory.apply(lambda x: len(x)) != 1] # TODO How do we want to handle these?

        # Remove duplicate emails
        dedup_mobile_res_dict = {}
        for i in common_email_res:
            if i['Mobile_Phone'] not in dedup_mobile_res_dict.keys():
                dedup_mobile_res_dict[i['Mobile_Phone']] = i
            # else:
                # logger.debug('Mobile_Phone %i already present' %(i['Mobile_Phone']))


        # Pull out only the columns that I want to actually load
        rename_columns = {'Email_Address':'EmailAddress'}
        cols = 'MobilePhoneScrubbed,Email_Address,DataSource,AudienceID,MessageID,SourceID,Channel,VersionNameArea,VersionNameCU,VersionNameAllUS,VersionNameTerritory,CreativeName,CreativeID,CampaignID,CID,CDOUpdate'.split(',')
        res = []
        for k, v in dedup_mobile_res_dict.items():
            if v['Suppression'] is None:
                temp_dict = {}
                for k2, v2 in v.items():
                    if k2 in cols:
                        if k2 in rename_columns.keys(): # Rename field if needed
                            temp_dict[rename_columns[k2]] = v2
                        else:
                            temp_dict[k2] = v2
                res.append(temp_dict)
            # else:
                # logger.debug('suppressed - %s' %(str(v)))
        # res = [v for k, v in dedup_mobile_res_dict.items()]
        return res
    except Exception as e:
        logger.error(e)
        raise e

if __name__=='__main__':
    from tabulate import tabulate
    import pandas as pd
    log_handler = logging.StreamHandler()
    log_handler.setLevel(logging.DEBUG)
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)

    df = pd.DataFrame(join())
    logging.info(tabulate(df.tail(100), headers='keys', tablefmt='psql'))

