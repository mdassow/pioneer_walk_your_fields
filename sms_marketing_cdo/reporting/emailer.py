import os
import pandas as pd
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')
from sms_marketing_cdo.config import config

class handler():
    """Generate email to send segment count summary results to end users specified in config file"""
    def generate_email(self, custom_text=None):
        try:
            sender_email = config[os.environ.get('python_env')]['summary_report_email_config']['from_address']
            receiver_email = config[os.environ.get('python_env')]['summary_report_email_config']['to_address_list']
            password = config[os.environ.get('python_env')]['smtp_server_config']['smtp_password']
            mailhost = config[os.environ.get('python_env')]['smtp_server_config']['email_host']
            port = config[os.environ.get('python_env')]['smtp_server_config']['email_host_port']
            user = config[os.environ.get('python_env')]['smtp_server_config']['smtp_user']

            message = MIMEMultipart("alternative")
            message["Subject"] = config[os.environ.get('python_env')]['summary_report_email_config']['email_subject']
            message["From"] = sender_email
            message["To"] = ','.join(receiver_email)

            # Create the plain-text and HTML version of your message
            text = """The SMS Load process has successfully ran: \n\n Custom Message:\n%s""" %(custom_text)
            # html = df.to_html()

            # Turn these into plain/html MIMEText objects
            part1 = MIMEText(text, "plain")
            # part2 = MIMEText(html, "html")

            # Add HTML/plain-text parts to MIMEMultipart message
            # The email client will try to render the last part first
            message.attach(part1)
            # message.attach(part2)

            smtp = smtplib.SMTP(mailhost, port)
            smtp.starttls()
            smtp.login(user=user, password=password)
            smtp.send_message(from_addr=sender_email, to_addrs=','.join(receiver_email), msg=message)
            logger.info('Summary email sent')
        except Exception as e:
            logger.error(e)
            raise(e)

if __name__=='__main__':
    # df = pd.DataFrame([{'test':'email'}])
    custom_text = 'This is a test. Someone is conducting a test of the Emergency Broadcast System. This is only a test.'
    h =handler()
    h.generate_email(custom_text=custom_text)