from datetime import datetime
import os
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import logging
from log_handling import run_log_handler, email_log_handler
from tabulate import tabulate

logger = logging.getLogger('Pioneer_Eloqua_API_App')
logger.setLevel(logging.DEBUG)
from models.run_log import run_log, qa_report
from sms_marketing_cdo.config import config

# Create MongoDB run log logging handler
r_log = run_log(account='sms_cdo'
                , channel='eloqua'
                , client_id=29
                , client_name='Pioneer'
                )
r_log.save()
handler = run_log_handler.run_log_handler(run_log_obj=r_log)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

# Set up email log handler
email_handler = email_log_handler.BufferingSMTPHandler(mailhost=config[os.environ.get('python_env')]['smtp_server_config']['email_host']
                                                       , mailport=config[os.environ.get('python_env')]['smtp_server_config']['email_host_port']
                                                       , fromaddr=config[os.environ.get('python_env')]['log_email_config']['from_address']
                                                       , toaddrs=config[os.environ.get('python_env')]['log_email_config']['to_address_list']
                                                       , subject=config[os.environ.get('python_env')]['log_email_config']['email_subject']
                                                       , smtp_user=config[os.environ.get('python_env')]['smtp_server_config']['smtp_user']
                                                       , smtp_password=config[os.environ.get('python_env')]['smtp_server_config']['smtp_password'])
email_handler.setLevel(logging.WARNING)
logger.addHandler(email_handler)

# create console handler with a different log level
ch = logging.StreamHandler()
# ch.setFormatter(fmt=logging.Formatter.formatStack)
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)

from sms_marketing_cdo.transform import sms_cdo
from sms_marketing_cdo.load import sms_cdo as sms_cdo_loader, sms_cdo_remove_old_records
from sms_marketing_cdo.reporting import emailer
# from sms_marketing_cdo.migrate_access_tables_to_sql_server.python_data_migrations import run_upload_support_tables
from api.eloqua_api.auth import refresh_token

def run() -> None:
    """Orchestrates the ETL process for Pioneer Eloqua Marketing CDO extract process"""
    try:
        # refresh_token.handle_auth_token_refresh()

        logger.info('run process has started')
        # run_upload_support_tables.run()
        # logger.info('input_cdo_df created')
        sms_data = sms_cdo.join()

        # write to file and/or API.
        sms_cdo_loader.write_data(data=sms_data)
        logger.info('sms_data complete')

        # Remove old records from import CDO that are no longer in current CDO (to keep in sync)
        sms_cdo_remove_old_records.handler(current_load_data=sms_data)

        logger.info('%s process has finished' %(__name__))

        qa_rep = qa_report(input_record_count=1, output_record_count=1)
        r_log.qa_report = qa_rep
        r_log.qa_status = 1
        r_log.run_end_datetime = datetime.now()
        r_log.save()

        emailer.handler().generate_email(custom_text='Total records uploaded: %i' %(len(sms_data)))
        logging.shutdown()

    except Exception as e:
        logger.error('%s process has failed with error %s' % (__name__, e))
        logging.shutdown()


if __name__=='__main__':
    run()