import os
import datetime
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from sms_marketing_cdo.config import config
from api.eloqua_api.contact_bulk_data import contact_export
import time
from datetime import datetime
import phonenumbers


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000))
        return result
    return timed

def get_from_api():
    try:
        export_def_id = config[os.environ.get('python_env')]['opt_in_contacts_export_def_id']
        data = contact_export.export_handler(export_def_id=export_def_id)
        # data = map_field_names(data=data)
        data = convert_d_types(data=data)
        assert len(data) > 0
        return data
    except Exception as e:
        logger.error(e)
        raise e

@timeit
def map_field_names(data):
    map_dict = {'Email_Address': 'Email Address'
                , 'Date_Created': 'Date Created'
                , 'Date_Modified': 'Date Modified'
    }
    res = []
    for l in data:
        temp_res = {}
        for k, v in l.items():
            if k in map_dict.keys():
                temp_res[map_dict[k]] = v
            else:
                temp_res[k] = v
        res.append(temp_res)

    return res

@timeit
def convert_d_types(data: list) -> list:
    data = list(map(convert_record, data))
    data = remove_phone_numbers_of_wrong_len(data, lens=[10])
    return data


def convert_record(record):
    record['Mobile_Opt_In'] = True if int(record['Mobile_Opt_In']) == 1 else False
    record['PHI_Company'] = True if int(record['PHI_Company']) == 1 else False
    record['Email_Address'] = record['Email_Address'].lower()
    record['Mobile_Phone_Raw'] = record['Mobile_Phone']
    record['Mobile_Phone'] = clean_up_phone_number(phone_number=record['Mobile_Phone'], country_code='US')
    return record

def clean_up_phone_number(phone_number, country_code='US'):
    try:
        phone_number = phone_number.replace('_','')
        formatted_number = phonenumbers.parse(phone_number, country_code)
        return formatted_number.national_number
    except Exception as e:
        print("phone number : %s is not valid due and had error: %s" %(phone_number, e))
        # raise e

def remove_phone_numbers_of_wrong_len(data, lens):
    res = []
    res_bad = []
    for i in data:
        if len(str(i['Mobile_Phone'])) in lens:
            res.append(i)
        else:
            res_bad.append(i)
    return res

@timeit
def get_data():
    logger.info('%s load_df has started' %(__name__))
    res = load_method()
    logger.info('%s load_df has finished' %(__name__))
    return res

def get_max(data, field_name):
    max = data[0][field_name]
    for i in data:
        if i[field_name] > max:
            max = i[field_name]
    return max

load_method = get_from_api

if __name__=='__main__':
    import pandas as pd
    log_handler = logging.StreamHandler()
    log_handler.setLevel(logging.DEBUG)
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)

    data = get_data()
    df = pd.DataFrame(data)
    logger.debug(df.head())
    logger.debug(df.columns)