import pandas as pd
import os
import datetime
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from sms_marketing_cdo.config import config
from api.eloqua_api.custom_object_bulk_data import cdo_export
import time
import phonenumbers


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000))
        return result
    return timed

def get_from_api():
    try:
        export_cdo_custom_object_id = config[os.environ.get('python_env')]['sms_log_cdo_custom_object_id']
        export_cdo_export_def_id = config[os.environ.get('python_env')]['sms_log_cdo_export_def_id']
        res = cdo_export.export_handler(custom_object_id=export_cdo_custom_object_id, export_def_id=export_cdo_export_def_id)
        res = convert_d_types(data=res)
        assert len(res) > 0
        return res
    except Exception as e:
        logger.error(e)
        raise e

def clean_up_phone_number(phone_number, country_code='US'):
    try:
        phone_number = phone_number.replace('_','')
        formatted_number = phonenumbers.parse(phone_number, country_code)
        return formatted_number.national_number
    except Exception as e:
        print("phone number : %s is not valid due and had error: %s" %(phone_number, e))
        # raise e


def remove_phone_numbers_of_wrong_len(data, lens):
    res = []
    res_bad = []
    for i in data:
        if len(str(i['From_Mobile_Phone'])) in lens:
            res.append(i)
        else:
            res_bad.append(i)
    return res

@timeit
def convert_d_types(data: list) -> list:
    data = list(map(convert_record, data))
    data = remove_phone_numbers_of_wrong_len(data, lens=[10])
    return data

def convert_record(record):
    # record['Mobile_Opt_In'] = True if int(record['Mobile_Opt_In']) == 1 else False
    # record['PHI_Company'] = True if int(record['PHI_Company']) == 1 else False
    # record['Email_Address'] = record['Email_Address'].lower()
    record['From_Mobile_Phone'] = clean_up_phone_number(phone_number=record['From1'], country_code='US')
    return record

def get_data():
    logger.info('%s load_df has started' %(__name__))
    data = load_method()
    # validate_df(df=res)
    logger.info('%s load_df has finished' %(__name__))
    return data

load_method = get_from_api


if __name__=='__main__':
    log_handler = logging.StreamHandler()
    log_handler.setLevel(logging.DEBUG)
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)

    data = get_data()
    df = pd.DataFrame(data)
    logger.debug(df.head())
    logger.debug(df.columns)