from os import sys, path
sys.path.append(path.dirname(path.abspath(__file__)))
import sql_server_connector


def query() -> list:
    """Pulls SMS_Version_CU records from SQL Server"""
    sql = """SELECT Sales_Structure.Terr_Code, Sales_Structure.CU_Num
            FROM Sales_Structure"""
    return sql_server_connector.handler().query(sql=sql)


if __name__ == "__main__":
    import logging
    import pandas as pd
    logger = logging.getLogger('Pioneer_Eloqua_API_App')
    log_handler = logging.StreamHandler()
    log_handler.setLevel(logging.DEBUG)
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)

    df = pd.DataFrame(query())
    logger.debug(df)


