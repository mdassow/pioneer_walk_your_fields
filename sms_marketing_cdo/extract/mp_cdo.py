import pandas as pd
import os
import datetime
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
from sms_marketing_cdo.config import config
from api.eloqua_api.custom_object_bulk_data import cdo_export
import time
from datetime import datetime

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000))
        return result
    return timed

def get_from_api():
    try:
        export_cdo_custom_object_id = config[os.environ.get('python_env')]['mp_cdo_export_cdo_custom_object_id']
        export_cdo_export_def_id = config[os.environ.get('python_env')]['mp_cdo_export_cdo_export_def_id']
        data = cdo_export.export_handler(custom_object_id=export_cdo_custom_object_id, export_def_id=export_cdo_export_def_id)
        # data = map_field_names(data=data)
        data = convert_d_types(data=data)
        assert len(data) > 0
        return data
    except Exception as e:
        logger.error(e)
        raise e

# @timeit
# def map_field_names(data):
#     return list(map(map_record, data))
#
# def map_record(record):
#     map_dict = {'Email_Address': 'Email Address'
#                 , 'Date_Created': 'Date Created'
#                 , 'Date_Modified': 'Date Modified'}
#     res = {}
#     for k, v in record.items():
#         if k in map_dict.keys():
#             res[map_dict[k]] = v
#         else:
#             res[k] = v
#     return res


# @timeit
# def convert_d_types(data):
#     print('map style')
#     return list(map(convert_record, data))
#
# def convert_record(record):
#     record['Date Created'] = datetime.strptime(record['Date Created'], '%Y-%m-%d %H:%M:%S.%f')
#     record['Date Modified'] = datetime.strptime(record['Date Modified'], '%Y-%m-%d %H:%M:%S.%f')
#     record['Email Address'] = record['Email Address'].lower()
#     return record

@timeit
def convert_d_types(data):
    print('loop style')
    for record in data:
        record['Date_Created'] = datetime.strptime(record['Date_Created'], '%Y-%m-%d %H:%M:%S.%f')
        record['Date_Modified'] = datetime.strptime(record['Date_Modified'], '%Y-%m-%d %H:%M:%S.%f')
        record['Email_Address'] = record['Email_Address'].lower()

    return data

@timeit
def get_data():
    logger.info('%s load_df has started' %(__name__))
    res = load_method()
    validate_df(data=res)
    logger.info('%s load_df has finished' %(__name__))
    return res

def validate_df(data):
    num_days = 1
    if not passes_check_last_changed_date(data=data, num_days=num_days):
        raise(Exception('extract cdo does not have a record updated in the past %i days' %(num_days)))

def passes_check_last_changed_date(data, num_days):
    max_last_modified = get_max(data=data, field_name='Date_Modified')
    time_delta = datetime.now() - max_last_modified
    if time_delta.days > num_days:
        return False
    return True

def get_max(data, field_name):
    max = data[0][field_name]
    for i in data:
        if i[field_name] > max:
            max = i[field_name]
    return max

load_method = get_from_api

if __name__=='__main__':
    log_handler = logging.StreamHandler()
    log_handler.setLevel(logging.DEBUG)
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)
    data = get_data()
    df = pd.DataFrame(data)
    logger.debug(df.head())
    logger.debug(df.columns)