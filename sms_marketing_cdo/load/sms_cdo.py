# import datetime
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')
from datetime import datetime

from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from api.eloqua_api.custom_object_bulk_data import cdo_import
from sms_marketing_cdo.config import config
import os


def write_to_csv(data, **kwargs):
    try:
        import pandas as pd
        df = pd.DataFrame(data)
        logger.info('%s write_to_csv has started' %(__name__))
        f_name = 'SMS_Manual_CDO_%s%s%s.csv' %(f'{datetime.today().month:02}', f'{datetime.today().day:02}', f'{datetime.today().year:04}')
        if 'before_filters' in kwargs.keys():
            if kwargs['before_filters']:
                fp = os.path.join(config[os.environ.get('python_env')]['cdo_export_dir'], 'output_before_filters', f_name)
            else:
                fp = os.path.join(config[os.environ.get('python_env')]['cdo_export_dir'], f_name)
        else:
            fp = os.path.join(config[os.environ.get('python_env')]['cdo_export_dir'], f_name)
        df.to_csv(fp, index=False)
        logger.info('%s write_to_csv has finished' %(__name__))
    except Exception as e:
        logger.error('%s write_to_csv has FAILED with error %s' %(__name__, e))

def write_to_many(data, **kwargs):
    method_list = [
        # write_to_excel,
        # write_to_csv,
        write_to_api]
    for method in method_list:
        if 'output_to' in kwargs.keys():
            if method.__name__ in kwargs['output_to']:
                method(data, **kwargs)
        else:
            method(data, **kwargs)


def write_to_api(data, **kwargs):
    """Gets cdo params from config and passes to load handler"""
    try:
        logger.info('%s write_to_api has started' %(__name__))
        import_cdo_custom_object_id = config[os.environ.get('python_env')]['import_cdo_custom_object_id']
        import_cdo_import_def_id = config[os.environ.get('python_env')]['import_cdo_import_def_id']
        cdo_import.import_handler(data, custom_object_id=import_cdo_custom_object_id, import_def_id=import_cdo_import_def_id)
        logger.info('%s write_to_api has finished' %(__name__))
    except Exception as e:
        logger.error('%s write_to_api has FAILED with error %s' %(__name__, e))
        raise(e)

def write_data(data, **kwargs):
    logger.info('%s write_data has started' %(__name__))
    return load_method(data=data, **kwargs)

load_method = write_to_many

if __name__=='__main__':
    import pandas as pd
    import json
    logger.setLevel(level=logging.DEBUG)

    def convert_to_json(data):
        try:
            output = []
            if type(data) is pd.DataFrame:
                for index, record in data.iterrows():
                    output.append(record.to_dict())
                return output
            elif type(data) is str:
                return data
            else:
                return json.dumps(data)
        except Exception as e:
            logger.error(e)
            raise e

    # create console handler with a different log level
    ch = logging.StreamHandler()
    # ch.setFormatter(fmt=logging.Formatter.formatStack)
    ch.setLevel(logging.DEBUG)
    logger.addHandler(ch)

    # root_dir = r'C:\Users\mdassow\Documents\Testing\Pioneer\Data Management\SMS CDO Uploads'
    # fp = os.path.join(root_dir, 'SMS_Manual_CDO_11282018.csv')
    # df = pd.read_csv(fp)
    # data = convert_to_json(df)
    # write_data(data=data)
    # logger.info(df.head())
    # logger.info(df.columns)