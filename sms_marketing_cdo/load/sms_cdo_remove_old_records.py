from os import sys, path
import os
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from sms_marketing_cdo.config import config
from api.eloqua_api.custom_object_bulk_data import cdo_export
from ratelimit import sleep_and_retry, limits
import logging
from tqdm import tqdm
logger = logging.getLogger('Pioneer_Eloqua_API_App')
from multiprocessing.pool import ThreadPool

from api.eloqua_api.custom_object_data_management import cdo_record_management
def get_from_api():
    try:
        import_cdo_custom_object_id = config[os.environ.get('python_env')]['import_cdo_custom_object_id']
        import_cdo_export_def_id = config[os.environ.get('python_env')]['import_cdo_export_def_id']
        data = cdo_export.export_handler(custom_object_id=import_cdo_custom_object_id, export_def_id=import_cdo_export_def_id)
        assert len(data) > 0
        return data
    except Exception as e:
        logger.error(e)
        raise e

# def compare(previous_load_data: pd.DataFrame, current_load_data: pd.DataFrame):
#     # Find email addresses that are no longer in data to be loaded
#     previous_load_data['source'] = 'previous_load'
#     current_load_data['source'] = 'current_load'
#     missing = previous_load_data.merge(current_load_data
#                                               , how='left'
#                                               , on=['EmailAddress']
#                                               , suffixes=('_previous', '_current'))
#     missing_ids = missing[pd.isnull(missing['source_current'])]['ID']
#     missing_emails = missing[pd.isnull(missing['source_current'])]['EmailAddress']
#     return missing_ids, missing_emails


def compare(previous_load_data: list, current_load_data: list) -> tuple:
    current_load_dict = {}
    missing_ids = []
    missing_MobilePhoneScrubbed = []
    for i in current_load_data:
        current_load_dict[str(i['MobilePhoneScrubbed'])] = i
    # Find email addresses that are no longer in data to be loaded

    for record in previous_load_data:
        try:
            temp = current_load_dict.pop(str(record['MobilePhoneScrubbed']))
            current_load_dict[str(i['MobilePhoneScrubbed'])] = temp
            # logger.debug('Found MobilePhoneScrubbed: %s' %(record['MobilePhoneScrubbed']))
        except KeyError:
            missing_MobilePhoneScrubbed.append(record['MobilePhoneScrubbed'])
            missing_ids.append(record['ID'])
        except Exception as e:
            logger.debug(e)
    return missing_ids, missing_MobilePhoneScrubbed

@sleep_and_retry
@limits(calls=100, period=3)
def delete(custom_object_id, id):
    cdo_record_management.delete_custom_object_record_data(custom_object_id=custom_object_id, id=id)
    logger.debug('Deleted record id: %s' %id)

def handler(current_load_data: list):
    # Pull Down DUPPEB.18015_WalkingYourFields_CDO
    logger.info('cdo_remove_old_records starting')
    previous_load_data = get_from_api()

    # Compare DUPPEB.18015_WalkingYourFields_CDO to data being loaded
    missing_records, missing_MobilePhoneScrubbed = compare(previous_load_data=previous_load_data, current_load_data=current_load_data)
    import_cdo_custom_object_id = config[os.environ.get('python_env')]['import_cdo_custom_object_id']
    pool = ThreadPool(10)
    if len(missing_records) > 0:
        logger.info('Removing records pertaining to emails %s' %(','.join(missing_MobilePhoneScrubbed)))
        for id in missing_records[:]: # Limiting max deletes to ensure we don't burn up API calls (Not sure what limit is)
            pool.apply_async(delete, args=(import_cdo_custom_object_id, id))
    pool.close()
    pool.join()
    logger.info('cdo_remove_old_records complete')
    return missing_records

if __name__=='__main__':
    import pandas as pd
    import json
    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    logger.addHandler(ch)


    def convert_to_json(data):
        try:
            if type(data) is pd.DataFrame:
                return data.to_json(orient='records')
            elif type(data) is str:
                return data
            else:
                return json.dumps(data)
        except Exception as e:
            logger.error(e)
            raise e

    # root_dir = r'C:\Users\mdassow\Documents\Testing\Pioneer\Data Management\SMS CDO Uploads'
    # fp = os.path.join(root_dir, 'SMS_Manual_CDO_12212018.csv')
    # fp = os.path.join(root_dir, 'SMS_Manual_CDO_11282018.csv')
    # df = pd.read_csv(fp)
    # data = json.loads(convert_to_json(df))
    # deleted_email_records = handler(current_load_data=data)