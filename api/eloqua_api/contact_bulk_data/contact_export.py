from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import logging
import json
import pandas as pd

from api.eloqua_api.sync_management import syncs

logger = logging.getLogger('Pioneer_Eloqua_API_App')


def export_handler(export_def_id: int) -> list:
    """Handles creating the sync call to stage data,
    waiting for sync to complete and retreives data.
    Cleans up sync after data is retrieved
    :rtype: python data frame"""
    logger.info('%s export_handler has started' %(__name__))
    # Create Export Sync
    create_sync_res = syncs.post_create_export_sync(contact_export_def_id=export_def_id)
    sync_id = json.loads(create_sync_res.content)['uri'].split('/')[2]

    # Check if sync is successful - waits until it is or it times out
    syncs.check_sync_successful(sync_id=sync_id, timeout=60)

    # Go get that data!
    res = syncs.paginate_get_sync_data(sync_id=sync_id, limit=10000)

    # Clean up - Delete sync data
    syncs.del_delete_sync(sync_id=sync_id)
    logger.info('%s export_handler has finished' %(__name__))

    return res

def export_as_df(export_def_id):
    """Wraps export_handler to return a data frame in desired format"""
    res_list = export_handler(export_def_id=export_def_id)
    df = pd.DataFrame(res_list)
    return df

if __name__=='__main__':
    from tabulate import tabulate
    logger.level = logging.DEBUG
    log_handler = logging.StreamHandler()
    log_handler.setLevel(logging.DEBUG)
    logger.addHandler(log_handler)

    res = export_handler(export_def_id=80105)
    df = pd.DataFrame(res)
    print(tabulate(df.head(100), headers='keys', tablefmt='psql'))