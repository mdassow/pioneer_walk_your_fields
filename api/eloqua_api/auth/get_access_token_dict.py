import json
from os import path
from connections import mongo
from datetime import datetime, timedelta
import requests


def load_auth_dict():
    return load_auth_dict_from_mongo()

def load_auth_dict_from_mongo(try_count=1):
    database = 'test'
    collection = 'clients'
    query_dict = {'access_tokens': {'$elemMatch': {'account': 'WYF'}}}
    projection = {"access_tokens.$.keys.access_tokens": 1}
    res = mongo.handler().query(database, collection, query_dict=query_dict, projection=projection)
    output = []
    for r in res:
        for at in r['access_tokens']:
            if at['account'] == 'WYF':
                if at['updated_at'] + timedelta(seconds=at['keys']['expires_in']) > datetime.now():
                    output.append(at['keys'])
                else:
                    requests.get('https://ab0dnzyrv2.execute-api.us-west-2.amazonaws.com/Prod/eloqua_get_cos_list?client_id=29&account=WYF')
                    if try_count < 3:
                        try_count += 1
                        return load_auth_dict_from_mongo(try_count=try_count)
                    else:
                        raise Exception('Unable to get updated auth token')
    if len(output) == 1:
        return output[0]
    else:
        raise Exception('Found %i records for auth token in MongoDB' %len(output))


def load_auth_dict_from_file():
    fp = path.join(path.dirname(path.abspath(__file__)), 'auth_token.json')
    file = open(fp, 'r')
    s = file.read()
    file.close()
    auth_dict = json.loads(s)
    return auth_dict

if __name__=='__main__':
    d = load_auth_dict()
    print(d)