import requests
import logging
import json
from datetime import datetime
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from api.eloqua_api.auth import get_access_token_dict
logger = logging.getLogger('Pioneer_Eloqua_API_App')

# create console handler with a higher log level
# ch = logging.StreamHandler()
# ch.setLevel(logging.DEBUG)
# logger.addHandler(ch)


def post_refresh_auth_token(refresh_token):
    try:
        url = "https://login.eloqua.com/auth/oauth2/token"
        params = {}
        headers = {"Content-Type": "application/json"
            , "Authorization":"Basic ZTU2ZjMxMDYtY2I3Yi00ODljLWEwNGItZmQyYWFjYTJmYjEwOjFJMWZRQ3dOQ3drYWtNWndQeVFtdDhhYWlid0UzVm5CVFdQTFhNOHM2ekViU3JSVkdZQ3Y2RXZWdGplUlBsWmFhcVRBVk1+M3FUZ0dYLVpYYnRIck1yazNxYmRiSDE3YklHanA="}# TODO remove header info
        json_data = {
                "grant_type": "refresh_token",
                "refresh_token": refresh_token,
                "scope": "full",
                "redirect_uri": "https://hexagon-staging.bader-rutter.com/eloqua_request_token_response"
                }
        data={}
        res = requests.post(url=url, params=params, data=data, json=json_data, headers=headers)
        # print(res.content)
        if res.status_code != 200:

            raise(Exception('status_code was %i. res: %s' %(res.status_code, str(res.reason))))
        # print(res)
        return res
    except Exception as e:
        logger.error(e)
        raise e

def load_refresh_token():
    auth_dict = get_access_token_dict.load_auth_dict()
    return auth_dict

def save_refresh_token_results(json_results):
    try:
        print(json_results)
        assert len(json_results) > 0 # Make sure json isn't empty
        assert 'refresh_token' in json_results.keys() # Make sure I have a refresh token before backing up
        assert 'access_token' in json_results.keys() # Make sure I have a access token before backing up

        fp = path.join(path.dirname(path.abspath(__file__)), 'auth_token.json')
        with open(fp, 'w') as myfile:
            myfile.write(json.dumps(json_results))
    except Exception as e:
        logger.error(e)
        fp = path.join(path.dirname(path.abspath(__file__)), 'FAILED auth_token %s.json' %(str(datetime.now())))
        with open(fp, 'w') as myfile:
            myfile.write(json.dumps(json_results))


def backup_previous_auth_token(json_results: dict):
    assert len(json_results) > 0 # Make sure json isn't empty
    assert 'refresh_token' in json_results.keys() # Make sure I have a refresh token before backing up
    assert 'access_token' in json_results.keys() # Make sure I have a access token before backing up

    fp = path.join(path.dirname(path.abspath(__file__)), 'auth_token_backup.json')
    with open(fp, 'w') as myfile:
        myfile.write(json.dumps(json_results))

def handle_auth_token_refresh():
    refresh_token = load_refresh_token()
    backup_previous_auth_token(refresh_token)
    refresh_res = post_refresh_auth_token(refresh_token=refresh_token['refresh_token'])
    json_res = refresh_res.json()
    save_refresh_token_results(json_results=json_res)

if __name__=='__main__':
    handle_auth_token_refresh()