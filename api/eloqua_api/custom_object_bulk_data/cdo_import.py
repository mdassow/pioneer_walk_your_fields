from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import pandas as pd
import requests
import logging
import json
from multiprocessing.pool import ThreadPool
from ratelimit import limits, sleep_and_retry
from tabulate import tabulate

from api.eloqua_api.sync_management import syncs
from api.eloqua_api.auth import get_access_token_dict
from ..custom_object_data_management import import_cdo_synced_record_handler

logger = logging.getLogger('Pioneer_Eloqua_API_App')
# logger.level = logging.DEBUG

# # create console handler with a higher log level
# ch = logging.StreamHandler()
# ch.setLevel(logging.DEBUG)
# logger.addHandler(ch)


class CountException(Exception):
    def __init__(self, *args, **kwargs):
        super(CountException, self).__init__(args, kwargs)


def convert_to_json(data):
    try:
        if type(data) is pd.DataFrame:
            return data.to_json(orient='records')
        elif type(data) is str:
            return data
        else:
            return json.dumps(data)
    except Exception as e:
        logger.error(e)
        raise e

@sleep_and_retry
@limits(calls=20, period=1)
def post_import_sync_data(data, custom_object_id, import_def_id, call_id='', try_count=1):
    try:
        auth_dict = get_access_token_dict.load_auth_dict()
        url = "https://secure.p03.eloqua.com/api/bulk/2.0/customObjects/%i/imports/%i/data" %(custom_object_id, import_def_id) #TODO Update to API endpoint
        json_data = convert_to_json(data=data)
        params = {'custom_object_id':custom_object_id, 'import_def_id':import_def_id}
        headers = {"Content-Type": "application/json"
            , "Authorization":"Bearer %s" %(auth_dict['access_token'])}# TODO remove header info
        res = requests.post(url=url, params=params, data=json_data, headers=headers)
        if res.status_code != 204:

            raise(Exception('status_code was %i. res: %s' %(res.status_code, str(res.reason))))
        logger.debug('Imported call_id %s successfully' %(str(call_id)))
        return res
    except Exception as e:
        try_count += 1
        if try_count <= 3:
            return post_import_sync_data(data=data, custom_object_id=custom_object_id, import_def_id=import_def_id, call_id=call_id, try_count=try_count)
        else:
            logger.error(e)
            raise(e)

def paginate_import_sync_data(data, custom_object_id, import_def_id, limit=1000, offset=0):
    pool = ThreadPool(20)
    call_id = 1
    while len(data) > limit + offset:
        temp_data = data[offset: offset + limit]
        # post_import_sync_data(data[offset: offset + limit], custom_object_id=custom_object_id, import_def_id=import_def_id)
        pool.apply_async(post_import_sync_data, args=(temp_data, custom_object_id, import_def_id, call_id))
        offset += limit
        call_id += 1
    pool.apply_async(post_import_sync_data, args=(data[offset: offset + limit], custom_object_id, import_def_id, call_id))
    pool.close()
    pool.join()

def import_handler(data, custom_object_id, import_def_id, try_count=1):
    try:
        """Handles calls to api to clear previously staged data, load new data into staging, sync data into destination CDO and verify success and check log records"""
        logger.info('%s import_handler has started' %(__name__))

        # Clear out any previously staged data
        import_cdo_synced_record_handler.del_delete_cdo_records(custom_object_id=custom_object_id, import_def_id=import_def_id)

        # Stage Data
        paginate_import_sync_data(data=data, custom_object_id=custom_object_id, import_def_id=import_def_id)


        # Create Sync
        create_sync_res = syncs.post_create_import_sync(custom_object_id=custom_object_id, import_def_id=import_def_id)
        sync_id = json.loads(create_sync_res.content)['uri'].split('/')[2]

        # Check if sync is finished
        syncs.check_sync_successful(sync_id=sync_id, timeout=60*60*3)


        # Check data len vs what made it to Eloqua CDO
        logs = syncs.get_sync_logs(sync_id=sync_id)
        total_records_log_found = False
        for l in logs['items']:
            if l['message'] == 'Total records staged for import.':
                total_records_log_found = True
                if len(data) != int(l['count']):
                    raise CountException('Total Records staged is incorrect. Attempted to stage %i, but %i arrived in staging' %(len(data), int(l['count'])))
                else:
                    logger.info('SyncID: %i - Total Records staged = %i. Expected Load = %i' %(int(sync_id), len(data), int(l['count'])))

        if not total_records_log_found:
            raise(Exception('Total Records staged for import log not present'))


        # Make sure that Eloqua didn't have issues getting staged data in to CDO
        get_retrieve_import_data_failure_res = syncs.get_retrieve_import_data_failure(sync_id)
        not_loaded_data = get_retrieve_import_data_failure_res['items']
        if len(not_loaded_data) > 0:
            logging.warning('Records were failed while trying to load into Eloqua. \n\n%s'%(tabulate(not_loaded_data, headers='keys',tablefmt='psql')))

        # Clean up - Delete sync data
        syncs.del_delete_sync(sync_id=sync_id)
        logger.info('%s import_handler has finished' %(__name__))
    except CountException as e:
        logger.error(e)
        if try_count <= 3:
            logger.error('Retrying loading data')
            try_count += 1
            import_handler(data=data, custom_object_id=custom_object_id, import_def_id=import_def_id, try_count=try_count)

    except TimeoutError as toe:
        logger.error(toe)

    except Exception as e:
        logger.error(e)
        if try_count <= 3:
            logger.error('Retrying loading data')
            try_count += 1
            import_handler(data=data, custom_object_id=custom_object_id, import_def_id=import_def_id, try_count=try_count)
        else:
            raise(e)



if __name__=='__main__':
    data = pd.read_json(r'C:\Users\mdassow\Downloads\eloqua_wyf_cdo (1).json').to_json(orient='records')
    res = import_handler(data=data, custom_object_id=336, import_def_id=23580)
    df = pd.DataFrame(res)
    print(tabulate(df.head(100), headers='keys', tablefmt='psql'))