import json
from pprint import pprint
# uses https://secure.p03.eloqua.com//api/bulk/2.0/customObjects/227/fields

field_json = json.loads("""{
    "items": [
        {
            "name": "AgronomistHeadshot1",
            "internalName": "HeadshotPath1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4529]}}",
            "uri": "/customObjects/227/fields/4529"
        },
        {
            "name": "AgronomistTwitterHandle1",
            "internalName": "IncludeForNorthernIA1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4531]}}",
            "uri": "/customObjects/227/fields/4531"
        },
        {
            "name": "AgronomistPhoneNumber1",
            "internalName": "HeadshotFlag1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4532]}}",
            "uri": "/customObjects/227/fields/4532"
        },
        {
            "name": "AgronomistWorkEmail1",
            "internalName": "TwitterHandle1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4533]}}",
            "uri": "/customObjects/227/fields/4533"
        },
        {
            "name": "AgronomistTitle1",
            "internalName": "PhoneNumber1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4534]}}",
            "uri": "/customObjects/227/fields/4534"
        },
        {
            "name": "AgronomistName1",
            "internalName": "Agronomist1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4537]}}",
            "uri": "/customObjects/227/fields/4537"
        },
        {
            "name": "Territories",
            "internalName": "Territories1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4538]}}",
            "uri": "/customObjects/227/fields/4538"
        },
        {
            "name": "District",
            "internalName": "District1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4539]}}",
            "uri": "/customObjects/227/fields/4539"
        },
        {
            "name": "CU",
            "internalName": "CU1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4541]}}",
            "uri": "/customObjects/227/fields/4541"
        },
        {
            "name": "EmailAddress",
            "internalName": "EmailAddress1",
            "dataType": "emailAddress",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4542]}}",
            "uri": "/customObjects/227/fields/4542"
        },
        {
            "name": "AgronomistHeadshotFilePath1",
            "internalName": "AgronomistHeadshotFilePath11",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4547]}}",
            "uri": "/customObjects/227/fields/4547"
        },
        {
            "name": "AgronomistName2",
            "internalName": "AgronomistName21",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4548]}}",
            "uri": "/customObjects/227/fields/4548"
        },
        {
            "name": "AgronomistTitle2",
            "internalName": "AgronomistTitle21",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4551]}}",
            "uri": "/customObjects/227/fields/4551"
        },
        {
            "name": "AgronomistWorkEmail2",
            "internalName": "AgronomistWorkEmail21",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4552]}}",
            "uri": "/customObjects/227/fields/4552"
        },
        {
            "name": "AgronomistPhoneNumber2",
            "internalName": "AgronomistPhoneNumber21",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4553]}}",
            "uri": "/customObjects/227/fields/4553"
        },
        {
            "name": "AgronomistTwitterHandle2",
            "internalName": "AgronomistTwitterHandle21",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4554]}}",
            "uri": "/customObjects/227/fields/4554"
        },
        {
            "name": "AgronomistHeadshot2",
            "internalName": "AgronomistHeadshot21",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4555]}}",
            "uri": "/customObjects/227/fields/4555"
        },
        {
            "name": "AgronomistHeadshotFilePath2",
            "internalName": "AgronomistHeadshotFilePath21",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4556]}}",
            "uri": "/customObjects/227/fields/4556"
        },
        {
            "name": "AgronomistName3",
            "internalName": "AgronomistName31",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4557]}}",
            "uri": "/customObjects/227/fields/4557"
        },
        {
            "name": "AgronomistTitle3",
            "internalName": "AgronomistTitle31",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4560]}}",
            "uri": "/customObjects/227/fields/4560"
        },
        {
            "name": "AgronomistWorkEmail3",
            "internalName": "AgronomistWorkEmail31",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4561]}}",
            "uri": "/customObjects/227/fields/4561"
        },
        {
            "name": "AgronomistPhoneNumber3",
            "internalName": "AgronomistPhoneNumber31",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4562]}}",
            "uri": "/customObjects/227/fields/4562"
        },
        {
            "name": "AgronomistTwitterHandle3",
            "internalName": "AgronomistTwitterHandle31",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4563]}}",
            "uri": "/customObjects/227/fields/4563"
        },
        {
            "name": "AgronomistHeadshot3",
            "internalName": "AgronomistHeadshot31",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4564]}}",
            "uri": "/customObjects/227/fields/4564"
        },
        {
            "name": "AgronomistHeadshotFilePath3",
            "internalName": "AgronomistHeadshotFilePath31",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4565]}}",
            "uri": "/customObjects/227/fields/4565"
        },
        {
            "name": "AgronomistCount",
            "internalName": "AgronomistCount1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4566]}}",
            "uri": "/customObjects/227/fields/4566"
        },
        {
            "name": "DataSource",
            "internalName": "SourceNotes1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4567]}}",
            "uri": "/customObjects/227/fields/4567"
        },
        {
            "name": "Area",
            "internalName": "Area1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4582]}}",
            "uri": "/customObjects/227/fields/4582"
        },
        {
            "name": "Territory",
            "internalName": "Territory1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4583]}}",
            "uri": "/customObjects/227/fields/4583"
        },
        {
            "name": "AudienceID",
            "internalName": "AudienceID1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4584]}}",
            "uri": "/customObjects/227/fields/4584"
        },
        {
            "name": "MessageID",
            "internalName": "MessageID1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4585]}}",
            "uri": "/customObjects/227/fields/4585"
        },
        {
            "name": "SourceID",
            "internalName": "SourceID1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4586]}}",
            "uri": "/customObjects/227/fields/4586"
        },
        {
            "name": "Channel",
            "internalName": "Channel1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4587]}}",
            "uri": "/customObjects/227/fields/4587"
        },
        {
            "name": "VersionName",
            "internalName": "VersionName1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4588]}}",
            "uri": "/customObjects/227/fields/4588"
        },
        {
            "name": "CreativeName",
            "internalName": "CreativeName1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4589]}}",
            "uri": "/customObjects/227/fields/4589"
        },
        {
            "name": "CreativeID",
            "internalName": "CreativeID1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4590]}}",
            "uri": "/customObjects/227/fields/4590"
        },
        {
            "name": "CampaignID",
            "internalName": "CampaignID1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4591]}}",
            "uri": "/customObjects/227/fields/4591"
        },
        {
            "name": "CID",
            "internalName": "CID1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4592]}}",
            "uri": "/customObjects/227/fields/4592"
        },
        {
            "name": "CC",
            "internalName": "CC1",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[4594]}}",
            "uri": "/customObjects/227/fields/4594"
        },
        {
            "name": "AgronomistName4",
            "internalName": "AgronomistName41",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[5688]}}",
            "uri": "/customObjects/227/fields/5688"
        },
        {
            "name": "AgronomistTitle4",
            "internalName": "AgronomistTitle41",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[5689]}}",
            "uri": "/customObjects/227/fields/5689"
        },
        {
            "name": "AgronomistWorkEmail4",
            "internalName": "AgronomistWorkEmail41",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[5690]}}",
            "uri": "/customObjects/227/fields/5690"
        },
        {
            "name": "AgronomistPhoneNumber4",
            "internalName": "AgronomistPhoneNumber41",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[5691]}}",
            "uri": "/customObjects/227/fields/5691"
        },
        {
            "name": "AgronomistTwitterHandle4",
            "internalName": "AgronomistTwitterHandle41",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[5692]}}",
            "uri": "/customObjects/227/fields/5692"
        },
        {
            "name": "AgronomistHeadshot4",
            "internalName": "AgronomistHeadshot41",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[5693]}}",
            "uri": "/customObjects/227/fields/5693"
        },
        {
            "name": "AgronomistHeadshotFilePath4",
            "internalName": "AgronomistHeadshotFilePath41",
            "dataType": "string",
            "hasReadOnlyConstraint": false,
            "hasNotNullConstraint": false,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].Field[5694]}}",
            "uri": "/customObjects/227/fields/5694"
        },
        {
            "name": "DataCard External Id",
            "internalName": "DataCardIDExt",
            "dataType": "string",
            "hasReadOnlyConstraint": true,
            "hasNotNullConstraint": true,
            "hasUniquenessConstraint": true,
            "statement": "{{CustomObject[227].ExternalId}}",
            "uri": ""
        },
        {
            "name": "DataCard Created At",
            "internalName": "CreatedAt",
            "dataType": "date",
            "hasReadOnlyConstraint": true,
            "hasNotNullConstraint": true,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].CreatedAt}}",
            "uri": ""
        },
        {
            "name": "DataCard Updated At",
            "internalName": "UpdatedAt",
            "dataType": "date",
            "hasReadOnlyConstraint": true,
            "hasNotNullConstraint": true,
            "hasUniquenessConstraint": false,
            "statement": "{{CustomObject[227].UpdatedAt}}",
            "uri": ""
        }
    ],
    "totalResults": 49,
    "limit": 1000,
    "offset": 0,
    "count": 49,
    "hasMore": false
}""")

output_dict = {}

for i in field_json['items']:
    output_dict[i["name"].replace(' ', '_')] = i["statement"]

print(json.dumps(output_dict))
