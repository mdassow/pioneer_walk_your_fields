import requests
import logging
import json
import time
import pandas as pd
from tabulate import tabulate
from os import sys, path
from multiprocessing.pool import ThreadPool

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from api.eloqua_api.auth import get_access_token_dict

logger = logging.getLogger('Pioneer_Eloqua_API_App')
# logger.level = logging.INFO

def post_create_export_sync(try_count=1, *args, **kwargs):
    try:
        auth_dict = get_access_token_dict.load_auth_dict()
        url = "https://secure.p03.eloqua.com/api/bulk/2.0/syncs" #TODO Update to API endpoint
        params = {}
        headers = {"Content-Type": "application/json"
            , "Authorization":"Bearer %s" %(auth_dict['access_token'])}# TODO remove header info

        if 'custom_object_id' in kwargs.keys():
            data = json.dumps({"syncedInstanceUri": "/customObjects/%i/exports/%i" %(kwargs['custom_object_id'], kwargs['export_def_id'])})
        elif 'contact_export_def_id' in kwargs.keys():
            data = json.dumps({"syncedInstanceUri": "/contacts/exports/%i" %(kwargs['contact_export_def_id'])})
        else:
            raise Exception('syncedInstanceUri type not recognized')

        res = requests.post(url=url, params=params, data=data, headers=headers)
        if res.status_code != 201:

            raise(Exception('status_code was %i. res: %s' %(res.status_code, str(res.reason))))
        return res
    except Exception as e:
        if try_count <= 3:
            try_count += 1
            time.sleep(3)
            return post_create_export_sync(try_count=try_count, *args, **kwargs)
        else:
            logger.error(e)
            raise e


def post_create_import_sync(custom_object_id, import_def_id):
    try:
        auth_dict = get_access_token_dict.load_auth_dict()
        url = "https://secure.p03.eloqua.com/api/bulk/2.0/syncs" #TODO Update to API endpoint
        params = {}
        headers = {"Content-Type": "application/json"
            , "Authorization":"Bearer %s" %(auth_dict['access_token'])}# TODO remove header info
        data = json.dumps({"syncedInstanceUri": "/customObjects/%i/imports/%i" %(custom_object_id, import_def_id)})
        res = requests.post(url=url, params=params, data=data, headers=headers)
        if res.status_code != 201:

            raise(Exception('status_code was %i. res: %s' %(res.status_code, str(res.reason))))
        return res
    except Exception as e:
        logger.error(e)
        raise e


def get_retrieve_sync_info(sync_id, try_count=1):
    try:
        auth_dict = get_access_token_dict.load_auth_dict()
        url = "https://secure.p03.eloqua.com/api/bulk/2.0/syncs/%s" %(sync_id) #TODO Update to API endpoint
        params = {'sync_id':sync_id}
        headers = {"Content-Type": "application/json"
            , "Authorization":"Bearer %s" %(auth_dict['access_token'])}# TODO remove header info
        data = {}
        res = requests.get(url=url, params=params, data=data, headers=headers)
        if res.status_code != 200:
            raise(Exception('status_code was %i' %(int(res.status_code))))
        return res
    except Exception as e:
        try_count += 1
        if try_count <= 3:
            time.sleep(3)
            return get_retrieve_sync_info(sync_id=sync_id, try_count=try_count)
        else:
            logger.error(e)
            raise e

def check_sync_successful(sync_id, timeout=600):
    try:
        start_time = time.time()
        logger.debug('starting check_sync_successful')
        while (time.time() - start_time <= timeout):
            res = get_retrieve_sync_info(sync_id=sync_id)
            if json.loads(res.content)['status'] == "success":
                return True
            if json.loads(res.content)['status'] == "active":
                logger.debug('sync is active and processing. Elapsed time: %i seconds'%(time.time() - start_time))
            elif json.loads(res.content)['status'] == "warning":
                logs = get_sync_logs(sync_id=sync_id)
                log_df = pd.DataFrame(logs['items'])
                logger.warning('Sync ending with Warning status. Logs:\n\n %s' %(tabulate(log_df, headers='keys', tablefmt='psql')))
                return True
            elif json.loads(res.content)['status'] == "error":
                logs = get_sync_logs(sync_id=sync_id)
                log_df = pd.DataFrame(logs['items'])
                del_delete_sync(sync_id=sync_id)
                raise Exception('Sync Failed. Logs:\n\n %s' %(tabulate(log_df, headers='keys', tablefmt='psql')))
            else:
                logger.debug('Status is %s for sync_id: %i. Elapsed time: %i seconds' %(json.loads(res.content)['status'], int(sync_id), time.time() - start_time))
            time.sleep(1)
        raise(TimeoutError('check_sync_successful timed out after %i seconds'%(timeout)))

    except Exception as e:
        logger.error(e)
        raise(e)

def del_delete_sync(sync_id):
    try:
        auth_dict = get_access_token_dict.load_auth_dict()
        url = "https://secure.p03.eloqua.com/api/bulk/2.0/syncs/%s/data" %(sync_id) #TODO Update to API endpoint
        params = {'sync_id':sync_id}
        headers = {"Content-Type": "application/json"
            , "Authorization":"Bearer %s" %(auth_dict['access_token'])}# TODO remove header info
        data = {}
        res = requests.delete(url=url, params=params, data=data, headers=headers)
        if res.status_code != 204:

            raise(Exception('status_code was %i. res: %s' %(res.status_code, str(res.reason))))
        return res
    except Exception as e:
        logger.error(e)
        raise e

def get_sync_logs(sync_id):
    try:
        auth_dict = get_access_token_dict.load_auth_dict()
        url = "https://secure.p03.eloqua.com/api/bulk/2.0/syncs/%s/logs" %(sync_id) #TODO Update to API endpoint
        params = {'sync_id':sync_id}
        headers = {"Content-Type": "application/json"
            , "Authorization":"Bearer %s" %(auth_dict['access_token'])}# TODO remove header info
        data = {}
        res = requests.get(url=url, params=params, data=data, headers=headers)
        if res.status_code != 200:
            raise(Exception('status_code was %i. res: %s' %(res.status_code, str(res.reason))))
        return json.loads(res.content)
    except Exception as e:
        logger.error(e)
        raise e

def get_retrieve_import_data_failure(sync_id):
    try:
        auth_dict = get_access_token_dict.load_auth_dict()
        url = "https://secure.p03.eloqua.com/api/bulk/2.0/syncs/%s/rejects" %(sync_id) #TODO Update to API endpoint
        params = {'sync_id':sync_id}
        headers = {"Content-Type": "application/json"
            , "Authorization":"Bearer %s" %(auth_dict['access_token'])}# TODO remove header info
        data = {}
        res = requests.get(url=url, params=params, data=data, headers=headers)
        if res.status_code != 200:

            raise(Exception('status_code was %i. res: %s' %(res.status_code, str(res.reason))))
        return json.loads(res.content)
    except Exception as e:
        logger.error(e)
        raise e


def get_sync_data(sync_id, limit=1000, offset=0, try_count=1):
    try:
        auth_dict = get_access_token_dict.load_auth_dict()
        url = "https://secure.p03.eloqua.com/api/bulk/2.0/syncs/%s/data" %(sync_id) #TODO Update to API endpoint
        params = {'sync_id': sync_id, 'limit':limit, 'offset':offset}
        headers = {"Content-Type": "application/json"
            , "Authorization":"Bearer %s" %(auth_dict['access_token'])}# TODO remove header info
        data = {}
        res = requests.get(url=url, params=params, data=data, headers=headers)
        if res.status_code != 200:

            raise(Exception('status_code was %i. res: %s' %(res.status_code, str(res.reason))))
        return res
    except Exception as e:
        try_count += 1
        if try_count <= 3:
            return get_sync_data(sync_id=sync_id, limit=limit, offset=offset, try_count=try_count)
        else:
            logger.error(e, exc_info=True)
            raise e


# def paginate_get_sync_data(sync_id, limit=1000, offset=0):
#     try:
#         output_items = []
#         res = get_sync_data(sync_id=sync_id, limit=limit, offset=0)
#         total_results = json.loads(res.content)['totalResults']
#         while total_results > limit + offset:
#             res = get_sync_data(sync_id=sync_id, limit=limit, offset=offset)
#             temp_items = json.loads(res.content)['items']
#             output_items.extend(temp_items)
#             offset += limit
#             logger.debug('Exported %i records' %(offset))
#         res = get_sync_data(sync_id=sync_id, limit=limit, offset=offset)
#         temp_items = json.loads(res.content)['items']
#         output_items.extend(temp_items)
#         logger.debug('Exported %i records' %(len(output_items)))
#
#         assert len(output_items) == total_results
#
#         return output_items
#
#     except Exception as e:
#         logger.error(e)
#         raise(e)

def paginate_get_sync_data(sync_id, limit=1000, offset=0):
    def call_back_handler(res):
        temp_items = json.loads(res.content)['items']
        output_items.extend(temp_items)
        logger.debug('Exported %i records' % (len(output_items)))

    try:

        pool = ThreadPool(10)
        output_items = []
        res = get_sync_data(sync_id=sync_id, limit=limit, offset=0)
        total_results = json.loads(res.content)['totalResults']
        while total_results > limit + offset:
            pool.apply_async(get_sync_data, args=(sync_id, limit, offset), callback=call_back_handler)
            offset += limit
        pool.apply_async(get_sync_data, args=(sync_id, limit, offset), callback=call_back_handler)
        pool.close()
        pool.join()
        logger.debug('Exported %i records' % (len(output_items)))
        logger.debug('total records to be exported: %s' % (str(total_results)))
        assert len(output_items) == total_results

        return output_items

    except Exception as e:
        logger.error(e)
        raise(e)


if __name__=='__main__':
    # from tabulate import tabulate
    # check_sync_successful(sync_id=108850, timeout=10000)

    res = get_sync_logs(sync_id=157440)
    print(tabulate(res['items'], headers='keys', tablefmt='psql'))
    # res_dropped_data = get_retrieve_import_data_failure(sync_id=108824)