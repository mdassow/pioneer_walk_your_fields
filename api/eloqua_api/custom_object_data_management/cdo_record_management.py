from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import requests
import logging
import json


from api.eloqua_api.sync_management import syncs
from api.eloqua_api.auth import get_access_token_dict

logger = logging.getLogger('Pioneer_Eloqua_API_App')


def get_cdo_data_list(custom_object_id: int, count: int = 1000, search_field: str = None, search_comparison_operator: str = None, search_value: str = None):
    try:
        auth_dict = get_access_token_dict.load_auth_dict()
        url = "https://secure.p03.eloqua.com/api/REST/2.0/data/customObject/%i/instances" %int(custom_object_id) #TODO Update to API endpoint
        params = {'custom_object_id': custom_object_id, 'count':count, 'search':'{}{}{}'.format(search_field, search_comparison_operator, search_value)}
        headers = {"Authorization":"Bearer %s" %(auth_dict['access_token'])}# TODO remove header info
        data = {}
        res = requests.get(url=url, params=params, data=data, headers=headers)
        if res.status_code != 200:

            raise(Exception('status_code was %i. res: %s' %(res.status_code, str(res.reason))))
        records = json.loads(res.content)['elements']
        return records
    except Exception as e:
        logger.error(e)
        raise e

def delete_custom_object_record_data(custom_object_id: int, id: int):
    try:
        auth_dict = get_access_token_dict.load_auth_dict()
        url = "https://secure.p03.eloqua.com/api/REST/2.0/data/customObject/%i/instance/%i" %(int(custom_object_id), int(id)) #TODO Update to API endpoint
        params = {'custom_object_id': custom_object_id, 'id':id}
        headers = {"Authorization":"Bearer %s" %(auth_dict['access_token'])}# TODO remove header info
        data = {}
        res = requests.delete(url=url, params=params, data=data, headers=headers)
        if res.status_code != 200:

            raise(Exception('status_code was %i. res: %s' %(res.status_code, str(res.reason))))
        return res
    except Exception as e:
        logger.error(e)
        raise e

if __name__=='__main__':
    res = get_cdo_data_list(custom_object_id=337)
    for d in res:
        del_res = delete_custom_object_record_data(custom_object_id=337, id=d['id'])
        print(del_res)