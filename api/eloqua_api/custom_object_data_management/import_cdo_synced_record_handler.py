import requests
import logging
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from api.eloqua_api.auth import get_access_token_dict

logger = logging.getLogger('Pioneer_Eloqua_API_App')


def del_delete_cdo_records(custom_object_id, import_def_id):
    try:
        auth_dict = get_access_token_dict.load_auth_dict()
        url = "https://secure.p03.eloqua.com/api/bulk/2.0/customObjects/%i/imports/%i/data" %(custom_object_id, import_def_id) #TODO Update to API endpoint
        params = {'custom_object_id':custom_object_id, 'import_def_id':import_def_id}
        headers = {"Content-Type": "application/json"
            , "Authorization":"Bearer %s" %(auth_dict['access_token'])}# TODO remove header info
        data = {}
        res = requests.delete(url=url, params=params, data=data, headers=headers)
        if res.status_code != 204:
            raise(Exception('status_code was %i. res: %s' %(res.status_code, str(res.reason))))
        return res
    except Exception as e:
        logger.error(e)
        raise e

if __name__=='__main__':
    res = del_delete_cdo_records(custom_object_id=336, import_def_id=23580)
    print(res)