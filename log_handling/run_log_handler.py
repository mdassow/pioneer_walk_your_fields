from logging import Handler
import json
from singleton_decorator import singleton

@singleton
class run_log_handler(Handler):
    def __init__(self, run_log_obj):
        super().__init__()
        self.run_log_obj = run_log_obj

    def emit(self, record):
        self.run_log_obj.add_process_log(data=str(record.msg)
                                            , log_level = record.levelno
                                            , error=str(record.msg) if record.levelno >= 30 else None
                                            , backtrace=record.stack_info
                                            , event_name=json.dumps({'file_path': record.pathname, 'func_name': record.funcName})
                                         )

# TODO the singleton above is not thread safe. I want to try this idea at some point:
# http://alacret.blogspot.com/2015/04/python-thread-safe-singleton-pattern.html
# For now I am running single threaded as a work around in the load\sms_cdo.py write_to_many func