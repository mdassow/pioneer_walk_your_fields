#!/usr/bin/env python
#
# Copyright 2001-2002 by Vinay Sajip. All Rights Reserved.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose and without fee is hereby granted,
# provided that the above copyright notice appear in all copies and that
# both that copyright notice and this permission notice appear in
# supporting documentation, and that the name of Vinay Sajip
# not be used in advertising or publicity pertaining to distribution
# of the software without specific, written prior permission.
# VINAY SAJIP DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
# ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
# VINAY SAJIP BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
# ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
# IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
# OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# This file is part of the Python logging distribution. See
# http://www.red-dove.com/python_logging.html
#
"""Test harness for the logging module. Tests BufferingSMTPHandler, an alternative implementation
of SMTPHandler.
Copyright (C) 2001-2002 Vinay Sajip. All Rights Reserved.
"""
import os
import logging, logging.handlers


class BufferingSMTPHandler(logging.handlers.BufferingHandler):
    def __init__(self, mailhost, mailport, fromaddr, toaddrs, subject, smtp_user, smtp_password, capacity=None):
        capacity = 100 if capacity is None else capacity
        logging.handlers.BufferingHandler.__init__(self, capacity)
        self.mailhost = mailhost
        self.mailport = mailport
        self.fromaddr = fromaddr
        self.toaddrs = toaddrs
        self.subject = subject
        self.smtp_user = smtp_user
        self.smtp_password = smtp_password
        self.setFormatter(logging.Formatter("%(asctime)s %(levelname)-5s %(message)s"))

    def emit(self, record):
        super().emit(record=record)

    def flush(self):
        if len(self.buffer) > 0:
            try:
                import smtplib
                port = self.mailport
                if not port:
                    port = smtplib.SMTP_PORT
                smtp = smtplib.SMTP(self.mailhost, port)
                smtp.starttls()
                smtp.login(user=self.smtp_user, password=self.smtp_password)
                msg = "From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n" % (self.fromaddr, ','.join(self.toaddrs), self.subject)
                for record in reversed(self.buffer):
                    s = self.format(record)
                    print(s)
                    msg = msg + s + "\r\n"
                smtp.sendmail(self.fromaddr, self.toaddrs, msg)
                smtp.quit()
            except Exception as e:
                print(e)
                self.handleError(None)  # no particular record
            self.buffer = []

def test():
    MAILHOST = 'smtp.office365.com'
    PORT = 587
    FROM = 'mdassow@bader-rutter.com'
    TO = ['mdassow@bader-rutter.com', 'dassowmd@gmail.com']
    SUBJECT = 'Test Logging email from Python logging module (buffering)'
    USER = 'mdassow@bader-rutter.com'
    PW = input('Enter your password')
    CAPACITY = 100
    logger = logging.getLogger("")
    logger.setLevel(logging.DEBUG)
    logger.addHandler(BufferingSMTPHandler(mailhost=MAILHOST, mailport=PORT, fromaddr=FROM, toaddrs=TO, subject=SUBJECT, smtp_user=USER, smtp_password=PW, capacity=CAPACITY))
    for i in range(102):
        logger.info("Info index = %d", i)
    logging.shutdown()

if __name__ == "__main__":
    test()