from multiprocessing.pool import ThreadPool
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import pandas as pd
from tqdm import tqdm
import logging
logger = logging.getLogger('Pioneer_WYF')

import access_connector
import sql_server_connector

def run(try_count=1):
    try:
        logger.info('Starting Terr_Seg Upload Process')
        df = query_access_data()
        truncate_sql_server()
        load_to_sql_server(df=df)
        logger.info('Finished Terr_Seg Upload Process')
    except Exception as e:
        logger.warning(e)
        import time; time.sleep(600)
        try_count += 1
        if try_count <= 3:
            run(try_count=try_count)
        else:
            raise(e)

def query_access_data():
    df = access_connector.query(sql="""SELECT ID, CU, Area, District, Territory, Terr_Code
                                        , `NEW 2019 SegmentName` as Segment, SegmentName, Status
                                        FROM Terr_Seg""", env='prod')
    return df

def truncate_sql_server():
    sql="""TRUNCATE TABLE Terr_Seg"""
    sql_server_connector.handler().insert(sql=sql)

def load_to_sql_server(df):
    try:
        pool = ThreadPool(10)
        sql_server_obj = sql_server_connector.handler(pool_size=10, max_overflow=10)
        for k, v in tqdm(df.iterrows(), total=len(df)):
            if v['Status'] is None or v['Status'] == 'active':
                is_active = 1
            else:
                is_active = 0
            sql = """INSERT INTO Pioneer.dbo.Terr_Seg
                    (ID, CU, Area, District, Territory, Terr_Code, Segment, SegmentName, is_active)
                    VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s);"""%(
                int(v['ID']) if not pd.isnull(v['ID']) else 'NULL'
                , int(v['CU']) if not pd.isnull(v['CU']) else 'NULL'
                , "'%s'"%(v['Area']) if not pd.isnull(v['Area']) else 'NULL'
                , "'%s'"%(v['District']) if not pd.isnull(v['District']) else 'NULL'
                , "'%s'"%(v['Territory']) if not pd.isnull(v['Territory']) else 'NULL'
                , "'%s'"%(v['Terr_Code']) if not pd.isnull(v['Terr_Code']) else 'NULL'
                , "'%s'"%(v['Segment']) if not pd.isnull(v['Segment']) else 'NULL'
                , "'%s'"%(v['SegmentName']) if not pd.isnull(v['SegmentName']) else 'NULL'
                , is_active
            )

            pool.apply_async(sql_server_obj.insert, args=(sql,))
        pool.close()
        pool.join()
    except Exception as e:
        raise e

if __name__=='__main__':
    run()