from multiprocessing.pool import ThreadPool
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import pandas as pd
from tqdm import tqdm
import logging
logger = logging.getLogger('Pioneer_WYF')

import access_connector
import sql_server_connector

def run(try_count=1):
    try:
        logger.info('Starting WYF_Seeds Upload Process')
        df = query_access_data()
        truncate_sql_server()
        load_to_sql_server(df=df)
        logger.info('Finished WYF_Seeds Upload Process')
    except Exception as e:
        logger.warning(e)
        import time; time.sleep(600)
        try_count += 1
        if try_count <= 3:
            run(try_count=try_count)
        else:
            raise(e)

def query_access_data():
    df = access_connector.query(sql="""SELECT *
                                    FROM WYF_Seeds""", env='prod')
    return df

def truncate_sql_server():
    sql="""TRUNCATE TABLE WYF_Seeds"""
    sql_server_connector.handler().insert(sql=sql)

def load_to_sql_server(df):
    try:
        pool = ThreadPool(10)
        sql_server_obj = sql_server_connector.handler(pool_size=10, max_overflow=10)
        for k, v in tqdm(df.iterrows(), total=len(df)):
            is_active = 1
            sql = """INSERT INTO WYF_Seeds
                    (email_address, message_id, is_active)
                    VALUES(%s, %s, %s);
                    """ %("'%s'"%(v['EmailAddress']) if not pd.isnull(v['EmailAddress']) else 'NULL'
                            , "'%s'"%(v['MessageID']) if not pd.isnull(v['MessageID']) else 'NULL'
                            , is_active)

            pool.apply_async(sql_server_obj.insert, args=(sql,))
        pool.close()
        pool.join()
    except Exception as e:
        raise e

if __name__=='__main__':
    run()