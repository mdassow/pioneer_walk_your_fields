from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import pandas as pd
from tqdm import tqdm
import logging
logger = logging.getLogger('Pioneer_WYF')

import access_connector
import sql_server_connector

def run(try_count=1):
    try:
        logger.info('Starting SmokeTest Upload Process')
        df = query_access_data()
        truncate_sql_server()
        load_to_sql_server(df=df)
        logger.info('Finished SmokeTest Upload Process')
    except Exception as e:
        logger.warning(e)
        import time; time.sleep(600)
        try_count += 1
        if try_count <= 3:
            run(try_count=try_count)
        else:
            raise(e)

def query_access_data():
    df = access_connector.query(sql="""SELECT *
                                    FROM SmokeTest""", env='prod')
    return df

def truncate_sql_server():
    sql="""TRUNCATE TABLE SmokeTest"""
    sql_server_connector.handler().insert(sql=sql)

def load_to_sql_server(df):
    try:
        for k, v in tqdm(df.iterrows(), total=len(df)):
            if v['Status'] is None or v['Status'] == 'active':
                is_active = 1
            else:
                is_active = 0
            sql = """INSERT INTO Pioneer.dbo.SmokeTest
                    (email_address, CU, Area, District, Territory, Territories, CC, DataSource, AudienceID
                    , message_id, SourceID, Channel, CreativeID, CampaignID, CID, Status, is_active)
                    VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
                    """%("'%s'"%(v['Email Address']) if not pd.isnull(v['Email Address']) else 'NULL'
                            , int(v['CU']) if not pd.isnull(v['CU']) else 'NULL'
                            , "'%s'"%(v['Area']) if not pd.isnull(v['Area']) else 'NULL'
                            , "'%s'"%(v['District']) if not pd.isnull(v['District']) else 'NULL'
                            , "'%s'"%(v['Territory']) if not pd.isnull(v['Territory']) else 'NULL'
                            , "'%s'"%(v['Territories']) if not pd.isnull(v['Territories']) else 'NULL'
                            , "'%s'"%(v['CC']) if not pd.isnull(v['CC']) else 'NULL'
                            , "'%s'"%(v['DataSource']) if not pd.isnull(v['DataSource']) else 'NULL'
                            , "'%s'"%(v['AudienceID']) if not pd.isnull(v['AudienceID']) else 'NULL'
                            , int(v['MessageID']) if not pd.isnull(v['MessageID']) else 'NULL'
                            , "'%s'"%(v['SourceID']) if not pd.isnull(v['SourceID']) else 'NULL'
                            , "'%s'"%(v['Channel']) if not pd.isnull(v['Channel']) else 'NULL'
                            , int(v['CreativeID']) if not pd.isnull(v['CreativeID']) else 'NULL'
                            , "'%s'"%(v['CampaignID']) if not pd.isnull(v['CampaignID']) else 'NULL'
                            , "'%s'"%(v['CID']) if not pd.isnull(v['CID']) else 'NULL'
                            , "'%s'"%(v['Status']) if not pd.isnull(v['Status']) else 'NULL'
                            , is_active)

            sql_server_connector.handler().insert(sql=sql)
    except Exception as e:
        raise e

if __name__=='__main__':
    run()