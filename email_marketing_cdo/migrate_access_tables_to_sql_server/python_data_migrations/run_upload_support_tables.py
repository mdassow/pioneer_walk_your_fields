from os import sys, path
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
from email_marketing_cdo.migrate_access_tables_to_sql_server.python_data_migrations import Previous_WYF_Seeds_Int_Ext
from email_marketing_cdo.migrate_access_tables_to_sql_server.python_data_migrations import segment_sender
from email_marketing_cdo.migrate_access_tables_to_sql_server.python_data_migrations import wyf_seeds, terr_seg
from email_marketing_cdo.migrate_access_tables_to_sql_server.python_data_migrations import signatures
import logging
logger = logging.getLogger('Pioneer_WYF')
from multiprocessing.pool import ThreadPool

# def run():
#     """Handles loading of SQL support tables from access database"""
#     logger.info('run_upload_support_tables has started')
#     signatures.run()
#     Previous_WYF_Seeds_Int_Ext.run()
#     segment_sender.run()
#     # smoke_test.run() # I don't think I need this data anymore because I'm deriving it
#     terr_seg.run()
#     wyf_seeds.run()
#     logger.info('run_upload_support_tables has finished')

# Multiprocessing
def run():
    """Handles loading of SQL support tables from access database"""
    try:
        logger.info('run_upload_support_tables has started')
        pool = ThreadPool(5)
        pool.apply_async(signatures.run)
        pool.apply_async(Previous_WYF_Seeds_Int_Ext.run())
        pool.apply_async(segment_sender.run())
        pool.apply_async(terr_seg.run())
        pool.apply_async(wyf_seeds.run())
        pool.close()
        pool.join()
        logger.info('run_upload_support_tables has finished')
    except Exception as e:
        try:
            pool.close()
            pool.join()
        except Exception as e2:
            logger.error(e2)

if __name__=='__main__':
    logger.setLevel(logging.INFO)
    run()