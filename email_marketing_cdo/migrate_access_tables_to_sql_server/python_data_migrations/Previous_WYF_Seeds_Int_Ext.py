from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import pandas as pd
from tqdm import tqdm
import logging
logger = logging.getLogger('Pioneer_WYF')
from multiprocessing.pool import ThreadPool
import access_connector
import sql_server_connector

def run(try_count=1):
    try:
        logger.info('Starting Previous_WYF_Seeds_Int_Ext Upload Process')
        df = query_access_data()
        truncate_sql_server()
        load_to_sql_server(df=df)
        logger.info('Finished Previous_WYF_Seeds_Int_Ext Upload Process')
    except Exception as e:
        logger.warning(e)
        import time; time.sleep(600)
        try_count += 1
        if try_count <= 3:
            run(try_count=try_count)
        else:
            raise(e)
def query_access_data():
    df = access_connector.query(sql="""SELECT *
                                    FROM Previous_WYF_Seeds_Int_Ext""", env='prod')
    return df

def truncate_sql_server():
    sql="""TRUNCATE TABLE Previous_WYF_Seeds_Int_Ext"""
    sql_server_connector.handler().insert(sql=sql)

def load_to_sql_server(df):
    try:
        pool = ThreadPool(10)
        sql_server_obj = sql_server_connector.handler(pool_size=10, max_overflow=10)
        for k, v in tqdm(df.iterrows(), total=len(df)):
            sql = """INSERT INTO Previous_WYF_Seeds_Int_Ext
                    (Status, [Mapped to Contact], EmailAddress, CC, DataSource, AudienceID, MessageID, SourceID, Channel, CampaignID, CID, [Eloqua Unique Identifier])
                    VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
                    """ %("'%s'"%(v['Status']) if not pd.isnull(v['Status']) else 'NULL'
                            , "'%s'"%(v['Mapped to Contact']) if not pd.isnull(v['Mapped to Contact']) else 'NULL'
                            , "'%s'"%(v['EmailAddress']) if not pd.isnull(v['EmailAddress']) else 'NULL'
                            , "'%s'"%(v['CC']) if not pd.isnull(v['CC']) else 'NULL'
                            , "'%s'"%(v['DataSource']) if not pd.isnull(v['DataSource']) else 'NULL'
                            , int(v['AudienceID']) if not pd.isnull(v['AudienceID']) else 'NULL'
                            , int(v['MessageID']) if not pd.isnull(v['MessageID']) else 'NULL'
                            , int(v['SourceID']) if not pd.isnull(v['SourceID']) else 'NULL'
                            , "'%s'"%(v['Channel']) if not pd.isnull(v['Channel']) else 'NULL'
                            , "'%s'"%(v['CampaignID']) if not pd.isnull(v['CampaignID']) else 'NULL'
                            , "'%s'"%(v['CID']) if not pd.isnull(v['CID']) else 'NULL'
                            , "'%s'"%(v['Eloqua Unique Identifier']) if not pd.isnull(v['Eloqua Unique Identifier']) else 'NULL'
                            )

            pool.apply_async(sql_server_obj.insert, args=(sql,))
        pool.close()
        pool.join()
    except Exception as e:
        raise e

if __name__=='__main__':
    run()