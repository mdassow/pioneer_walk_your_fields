from multiprocessing.pool import ThreadPool
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import pandas as pd
from tqdm import tqdm
import logging
logger = logging.getLogger('Pioneer_WYF')

import access_connector
import sql_server_connector

def run(try_count=1):
    try:
        logger.info('Starting Segment_Sender Upload Process')
        df = query_access_data()
        truncate_sql_server()
        load_to_sql_server(df=df)
        logger.info('Finished Segment_Sender Upload Process')
    except Exception as e:
        logger.warning(e)
        import time; time.sleep(600)
        try_count += 1
        if try_count <= 3:
            run(try_count=try_count)
        else:
            raise(e)

def query_access_data():
    df = access_connector.query(sql="""SELECT Status, SegmentName, AgronomistID1 as agronomist_id_1, AgronomistID2 as agronomist_id_2
                                    , AgronomistID3 as agronomist_id_3, AgronomistID4 as agronomist_id_4, CC
                                    , DataSource, AudienceID, MessageID, SourceID, Channel, VersionName, CreativeName
                                    , CreativeID, CampaignID, CID, AgronomistCount, Notes
                                    FROM Segment_Sender""", env='prod')
    return df

def truncate_sql_server():
    sql="""TRUNCATE TABLE Segment_Sender"""
    sql_server_connector.handler().insert(sql=sql)

def load_to_sql_server(df):
    try:
        pool = ThreadPool(10)
        sql_server_obj = sql_server_connector.handler(pool_size=10, max_overflow=10)
        for k, v in tqdm(df.iterrows(), total=len(df)):
            if v['Status'] is None or v['Status'] == 'active':
                is_active = 1
            else:
                is_active = 0
            sql = """INSERT INTO Segment_Sender
                    (SegmentName, CC, DataSource, AudienceID, MessageID, SourceID, Channel, VersionName, CreativeName
                    , CreativeID, CampaignID, CID, AgronomistCount, is_active, agronomist_id_1, agronomist_id_2
                    , agronomist_id_3, agronomist_id_4, Status, Notes)
                    VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
                    """ %("'%s'"%(v['SegmentName']) if not pd.isnull(v['SegmentName']) else 'NULL'
                            , "'%s'"%(v['CC']) if not pd.isnull(v['CC']) else 'NULL'
                            , "'%s'"%(v['DataSource']) if not pd.isnull(v['DataSource']) else 'NULL'
                            , "'%s'"%(v['AudienceID']) if not pd.isnull(v['AudienceID']) else 'NULL'
                            , int(v['MessageID']) if not pd.isnull(v['MessageID']) else 'NULL'
                            , "'%s'"%(v['SourceID']) if not pd.isnull(v['SourceID']) else 'NULL'
                            , "'%s'"%(v['Channel']) if not pd.isnull(v['Channel']) else 'NULL'
                            , "'%s'"%(v['VersionName']) if not pd.isnull(v['VersionName']) else 'NULL'
                            , "'%s'"%(v['CreativeName']) if not pd.isnull(v['CreativeName']) else 'NULL'
                            , int(v['CreativeID']) if not pd.isnull(v['CreativeID']) else 'NULL'
                            , "'%s'"%(v['CampaignID']) if not pd.isnull(v['CampaignID']) else 'NULL'
                            , "'%s'"%(v['CID']) if not pd.isnull(v['CID']) else 'NULL'
                            , "'%s'"%(v['AgronomistCount']) if not pd.isnull(v['AgronomistCount']) else 'NULL'
                            , is_active
                            , int(v['agronomist_id_1']) if not pd.isnull(v['agronomist_id_1']) else 'NULL'
                            , int(v['agronomist_id_2']) if not pd.isnull(v['agronomist_id_2']) else 'NULL'
                            , int(v['agronomist_id_3']) if not pd.isnull(v['agronomist_id_3']) else 'NULL'
                            , int(v['agronomist_id_4']) if not pd.isnull(v['agronomist_id_4']) else 'NULL'
                            , "'%s'"%(v['Status']) if not pd.isnull(v['Status']) else 'NULL'
                            , "'%s'"%(v['Notes']) if not pd.isnull(v['Notes']) else 'NULL')

            pool.apply_async(sql_server_obj.insert, args=(sql,))
        pool.close()
        pool.join()
    except Exception as e:
        raise e
if __name__=='__main__':
    run()