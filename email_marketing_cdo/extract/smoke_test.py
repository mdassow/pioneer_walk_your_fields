from os import sys, path
sys.path.append(path.dirname(path.abspath(__file__)))
import sql_server_connector
from datetime import datetime
import logging
import pandas as pd


def load_df() -> pd.DataFrame:
    """Pulls smoke test records from SQL Server"""
    sql = """SELECT
	CONCAT('WYF', ss.MessageID, '@test.com') AS EmailAddress,
	Terr_Seg.CU,
	NULL AS Area,
	MIN(Terr_Seg.District) AS District,
	MIN(Terr_Codes.[Territory Code]) AS Territory,
	STRING_AGG(Terr_Codes.[Territory Code],
	',') AS Territories,
	MAX(sig_1.Name) as AgronomistName1 ,
	MAX(sig_1.Title) as AgronomistTitle1 ,
	MAX(sig_1.Email) as AgronomistWorkEmail1 ,
	MAX(sig_1.Phone) as AgronomistPhoneNumber1 ,
	MAX(sig_1.Twitter) AS AgronomistTwitterHandle1 ,
    CASE WHEN ss.agronomist_id_1 IS NOT NULL THEN MAX(sig_1.[Headshot Flag])
        ELSE MAX(ssgh.[Headshot Flag])
    END AS AgronomistHeadshot1,
    CASE WHEN ss.agronomist_id_1 IS NOT NULL THEN MAX(sig_1.Headshot)
        ELSE MAX(ssgh.Headshot)
    END AS AgronomistHeadshotFilePath1,
	MAX(sig_2.Name) as AgronomistName2 ,
	MAX(sig_2.Title) as AgronomistTitle2 ,
	MAX(sig_2.Email) as AgronomistWorkEmail2 ,
	MAX(sig_2.Phone) as AgronomistPhoneNumber2 ,
	MAX(sig_2.Twitter) AS AgronomistTwitterHandle2 ,
	MAX(sig_2.[Headshot Flag]) AS AgronomistHeadshot2 ,
	MAX(sig_2.Headshot) AS AgronomistHeadshotFilePath2 ,
	MAX(sig_3.Name) as AgronomistName3 ,
	MAX(sig_3.Title) as AgronomistTitle3 ,
	MAX(sig_3.Email) as AgronomistWorkEmail3 ,
	MAX(sig_3.Phone) as AgronomistPhoneNumber3 ,
	MAX(sig_3.Twitter) AS AgronomistTwitterHandle3 ,
	MAX(sig_3.[Headshot Flag]) AS AgronomistHeadshot3 ,
	MAX(sig_3.Headshot) AS AgronomistHeadshotFilePath3 ,
	MAX(sig_4.Name) as AgronomistName4 ,
	MAX(sig_4.Title) as AgronomistTitle4 ,
	MAX(sig_4.Email) as AgronomistWorkEmail4 ,
	MAX(sig_4.Phone) as AgronomistPhoneNumber4 ,
	MAX(sig_4.Twitter) AS AgronomistTwitterHandle4 ,
	MAX(sig_4.[Headshot Flag]) AS AgronomistHeadshot4 ,
	MAX(sig_4.Headshot) AS AgronomistHeadshotFilePath4,
    CASE 
        WHEN ss.agronomist_id_4 IS NOT NULL THEN 4
        WHEN ss.agronomist_id_3 IS NOT NULL THEN 3
        WHEN ss.agronomist_id_2 IS NOT NULL THEN 2
        WHEN ss.agronomist_id_1 IS NOT NULL THEN 1
        ELSE 5 -- NOTE : 5 is used as a hack to indicate that the group headshot image should be used instead of personal ones
    END AS AgronomistCount,
	ss.CC,
	'3' AS AudienceID,
	ss.MessageID,
	'2' AS SourceID,
	ss.Channel,
	ss.VersionName,
	ss.CreativeName,
	ss.CreativeID,
	ss.CampaignID,
	ss.CID,
	MAX(CONVERT(int,Terr_Seg.is_active)) AS terr_seg_is_active, 
	MAX(CONVERT(int,ss.is_active)) AS segment_sender_is_active
FROM
	Terr_Codes
INNER JOIN Terr_Seg ON
	Terr_Codes.[Territory Code] = Terr_Seg.Terr_Code
INNER JOIN Segment_Sender ss ON
	Terr_Seg.Segment = ss.SegmentName
LEFT JOIN Pioneer.dbo.Signatures sig_1 ON
	ss.Agronomist_ID_1 = sig_1.ID
LEFT JOIN Pioneer.dbo.Signatures sig_2 ON
	ss.Agronomist_ID_2 = sig_2.ID
LEFT JOIN Pioneer.dbo.Signatures sig_3 ON
	ss.Agronomist_ID_3 = sig_3.ID
LEFT JOIN Pioneer.dbo.Signatures sig_4 ON
	ss.Agronomist_ID_4 = sig_4.ID
LEFT JOIN Segment_Sender_Group_Headshots ssgh ON 
    ss.SegmentName = ssgh.SegmentName
WHERE
    (ss.is_active=1
    AND (Terr_Seg.is_active=1))
	GROUP BY Terr_Seg.CU,
 ss.AgronomistCount,
	ss.agronomist_id_1,
	ss.agronomist_id_2,
	ss.agronomist_id_3,
	ss.agronomist_id_4,
	ss.CC,
	ss.MessageID,
	ss.Channel,
	ss.VersionName,
	ss.CreativeName,
	ss.CreativeID,
	ss.CampaignID,
	ss.CID,
	ss.SegmentName,
	ss.DataSource
ORDER BY
	ss.SegmentName;
"""
    df = sql_server_connector.handler().load_df(sql=sql)
    df['DataSource'] = 'Update_WYF_%s%s%s' %(f'{datetime.today().month:02}', f'{datetime.today().day:02}', f'{datetime.today().year:04}')
    return df


if __name__ == "__main__":
    df = load_df()
    logging.debug(df.head())