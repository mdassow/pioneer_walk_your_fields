from os import sys, path
sys.path.append(path.dirname(path.abspath(__file__)))
import sql_server_connector
import logging
import pandas as pd


def load_df() -> pd.DataFrame:
    """Pulls territory segment records from SQL Server"""
    sql = """SELECT * FROM Terr_Seg
                WHERE is_active = 1"""
    return sql_server_connector.handler().load_df(sql=sql)


if __name__ == "__main__":
    df = load_df()
    logging.debug(df.head())