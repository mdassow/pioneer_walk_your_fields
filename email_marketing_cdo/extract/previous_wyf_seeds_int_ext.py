from os import sys, path
sys.path.append(path.dirname(path.abspath(__file__)))
import sql_server_connector
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')
import pandas as pd


def load_df() -> pd.DataFrame:
    """Pulls previous wyf seeds int_ext from SQL Server"""
    sql = """SELECT * FROM Previous_WYF_Seeds_Int_Ext
                WHERE (((Previous_WYF_Seeds_Int_Ext.AudienceID) != 1))
                    OR (((Previous_WYF_Seeds_Int_Ext.SourceID) != 1))"""
    return sql_server_connector.handler().load_df(sql=sql)


if __name__ == "__main__":
    from tabulate import tabulate
    df = load_df()
    logger.setLevel(logging.INFO)
    logger.debug(tabulate(df.head(100), headers='keys', tablefmt='psql'))