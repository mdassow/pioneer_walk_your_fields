from os import sys, path
sys.path.append(path.dirname(path.abspath(__file__)))
import sql_server_connector
from datetime import datetime
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')

def load_df():
    """Pulls segment sender from SQL Server"""
    sql = """SELECT ss.SegmentName
    , CC
    , DataSource
    , '1' AS AudienceID
    , MessageID
    , '1' AS SourceID
    , Channel
    , VersionName
    , CreativeName
    , CreativeID
    , CampaignID
    , CID
    , ss.is_active as is_active
    , CASE 
        WHEN ss.agronomist_id_4 IS NOT NULL THEN 4
        WHEN ss.agronomist_id_3 IS NOT NULL THEN 3
        WHEN ss.agronomist_id_2 IS NOT NULL THEN 2
        WHEN ss.agronomist_id_1 IS NOT NULL THEN 1
        ELSE 5 -- NOTE : 5 is used as a hack to indicate that the group headshot image should be used instead of personal ones
    END AS agronomist_count
    , sig_1.Name as AgronomistName1
    , sig_1.Title as AgronomistTitle1	
    , sig_1.Email as AgronomistWorkEmail1
    , sig_1.Phone as AgronomistPhoneNumber1
    , sig_1.Twitter AS AgronomistTwitterHandle1
    , CASE WHEN ss.agronomist_id_1 IS NOT NULL THEN sig_1.[Headshot Flag]
        ELSE ssgh.[Headshot Flag]
        END AS AgronomistHeadshot1
    , CASE WHEN ss.agronomist_id_1 IS NOT NULL THEN sig_1.Headshot
        ELSE ssgh.Headshot
        END AS AgronomistHeadshotFilePath1
    , sig_2.Name as AgronomistName2
    , sig_2.Title as AgronomistTitle2
    , sig_2.Email as AgronomistWorkEmail2
    , sig_2.Phone as AgronomistPhoneNumber2
    , sig_2.Twitter AS AgronomistTwitterHandle2
    , sig_2.[Headshot Flag] AS AgronomistHeadshot2
    , sig_2.Headshot AS AgronomistHeadshotFilePath2
    , sig_3.Name as AgronomistName3
    , sig_3.Title as AgronomistTitle3
    , sig_3.Email as AgronomistWorkEmail3
    , sig_3.Phone as AgronomistPhoneNumber3
    , sig_3.Twitter AS AgronomistTwitterHandle3
    , sig_3.[Headshot Flag] AS AgronomistHeadshot3
    , sig_3.Headshot AS AgronomistHeadshotFilePath3
    , sig_4.Name as AgronomistName4
    , sig_4.Title as AgronomistTitle4
    , sig_4.Email as AgronomistWorkEmail4
    , sig_4.Phone as AgronomistPhoneNumber4
    , sig_4.Twitter AS AgronomistTwitterHandle4
    , sig_4.[Headshot Flag] AS AgronomistHeadshot4
    , sig_4.Headshot AS AgronomistHeadshotFilePath4
    FROM Segment_Sender  ss
    LEFT JOIN Pioneer.dbo.Signatures sig_1
    ON ss.Agronomist_ID_1 = sig_1.ID
    LEFT JOIN Pioneer.dbo.Signatures sig_2
    ON ss.Agronomist_ID_2 = sig_2.ID
    LEFT JOIN Pioneer.dbo.Signatures sig_3
    ON ss.Agronomist_ID_3 = sig_3.ID
    LEFT JOIN Pioneer.dbo.Signatures sig_4
    ON ss.Agronomist_ID_4 = sig_4.ID
    LEFT JOIN Segment_Sender_Group_Headshots ssgh 
    ON ss.SegmentName = ssgh.SegmentName
    WHERE ss.is_active = 1 
    ;"""
    df = sql_server_connector.handler().load_df(sql=sql)
    df['DataSource'] = 'Update_WYF_%s%s%s' % (f'{datetime.today().month:02}', f'{datetime.today().day:02}', f'{datetime.today().year:04}')
    return df

if __name__=="__main__":
    df = load_df()
    logging.debug(df.head())