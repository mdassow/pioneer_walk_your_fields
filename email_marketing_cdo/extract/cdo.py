import pandas as pd
import os
import datetime
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.dirname(path.abspath(__file__)))))
from email_marketing_cdo.config import config
from api.eloqua_api.custom_object_bulk_data import cdo_export
import time


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000))
        return result
    return timed

def get_from_api():
    try:
        export_cdo_custom_object_id = config[os.environ.get('python_env')]['export_cdo_custom_object_id']
        export_cdo_export_def_id = config[os.environ.get('python_env')]['export_cdo_export_def_id']
        df = cdo_export.export_as_df(custom_object_id=export_cdo_custom_object_id, export_def_id=export_cdo_export_def_id)
        df = map_field_names(df=df)
        df = convert_d_types(df=df)
        assert len(df) > 0
        return df
    except Exception as e:
        logger.error(e)
        raise e


@timeit
def map_field_names(df):
    map_dict = {'Email_Address': 'Email Address'
                , 'Date_Created': 'Date Created'
                , 'Date_Modified': 'Date Modified'
    }

    df.rename(columns=map_dict, inplace=True)
    return df

@timeit
def convert_d_types(df):
    df['Date Created'] = pd.to_datetime(df['Date Created'])
    df['Date Modified'] = pd.to_datetime(df['Date Modified'])
    df['Email Address'] = df['Email Address'].str.lower()
    return df

@timeit
def get_df():
    logger.info('%s load_df has started' %(__name__))
    res = load_method()
    validate_df(df=res)
    logger.info('%s load_df has finished' %(__name__))
    return res

def validate_df(df):
    num_days = 1
    if not passes_check_last_changed_date(df=df, num_days=num_days):
        raise(Exception('extract cdo does not have a record updated in the past %i days' %(num_days)))

def passes_check_last_changed_date(df, num_days):
    max_last_modified = df['Date Modified'].max()
    time_delta = datetime.datetime.now() - max_last_modified
    if time_delta.days > num_days:
        return False
    return True

load_method = get_from_api


if __name__=='__main__':
    log_handler = logging.StreamHandler()
    log_handler.setLevel(logging.DEBUG)
    logger.addHandler(log_handler)
    logger.setLevel(logging.DEBUG)
    df = get_df()
    logger.debug(df.head())
    logger.debug(df.columns)