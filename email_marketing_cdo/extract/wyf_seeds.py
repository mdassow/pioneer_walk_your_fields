from os import sys, path
sys.path.append(path.dirname(path.abspath(__file__)))
import sql_server_connector
import logging
import pandas as pd


def load_df() -> pd.DataFrame:
    """Pulls wyf seed records from SQL Server"""
    sql = """SELECT * FROM wyf_seeds
                WHERE is_active = 1"""
    df = sql_server_connector.handler().load_df(sql=sql)
    df['email_address'] = df['email_address'].str.lower()
    return df


if __name__ == "__main__":
    df = load_df()
    logging.debug(df.head())