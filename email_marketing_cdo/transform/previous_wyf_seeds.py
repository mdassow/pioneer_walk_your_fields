import pandas as pd
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')

from email_marketing_cdo.extract import wyf_seeds, terr_seg, cdo as cdo_extracter, cdo, segment_sender, \
    previous_wyf_seeds_int_ext


def join(input_cdo) -> pd.DataFrame:
    """Requests cdo extract, makes transformations, processes filters and returns data as pandas data frame"""
    try:
        # Update AudienceID and SourceID for anyone who is in the previous Previous_WYF_Seeds_Int_Ext table
        previous_wyf_seeds_int_ext_df = previous_wyf_seeds_int_ext.load_df()[['EmailAddress', 'AudienceID', 'SourceID']]
        previous_wyf_seeds_int_ext_df.rename(columns={'EmailAddress':'EmailAddressTemp'
                                                        , 'AudienceID': 'AudienceIDTemp'
                                                        ,'SourceID': 'SourceIDTemp'}, inplace=True)
        join_df = input_cdo.merge(previous_wyf_seeds_int_ext_df, how='left', left_on='EmailAddress', right_on='EmailAddressTemp')
        join_df.loc[pd.isnull(join_df['AudienceIDTemp']) == False, 'AudienceID'] = join_df['AudienceIDTemp']
        join_df.loc[pd.isnull(join_df['SourceIDTemp']) == False, 'SourceID'] = join_df['SourceIDTemp']
        join_df.drop(['EmailAddressTemp', 'AudienceIDTemp','SourceIDTemp'], axis=1, inplace=True)

        # join_df = process_filters(join_df=join_df)
        return join_df
    except Exception as e:
        raise e

def process_filters(join_df):
    join_df['filter_reason'] = ''

    # check_terr_seg_status
    join_df.loc[join_df['terr_seg_is_active'] == False, 'passed_terr_seg_status'] = False
    join_df.loc[join_df['terr_seg_is_active'] == False, 'filter_reason'] = join_df['filter_reason'] + 'Terr_Seg was not active,'
    join_df.loc[pd.isnull(join_df['terr_seg_is_active']), 'passed_terr_seg_status'] = False
    join_df.loc[pd.isnull(join_df['terr_seg_is_active']), 'filter_reason'] = join_df['filter_reason'] + 'Terr_Seg was NULL,'
    join_df.loc[join_df['terr_seg_is_active'] == True, 'passed_terr_seg_status'] = True

    # check_segment_sender_status
    join_df.loc[join_df['segment_sender_is_active'] == False, 'passed_segment_sender_status'] = False
    join_df.loc[join_df['segment_sender_is_active'] == False, 'filter_reason'] = join_df['filter_reason'] + 'Segment_Sender was not active,'
    join_df.loc[pd.isnull(join_df['segment_sender_is_active']), 'passed_segment_sender_status'] = False
    join_df.loc[pd.isnull(join_df['segment_sender_is_active']), 'filter_reason'] = join_df['filter_reason'] + 'Segment_Sender was NULL,'
    join_df.loc[join_df['segment_sender_is_active'] == True, 'passed_segment_sender_status'] = True

    return join_df

if __name__=='__main__':
    from tabulate import tabulate
    logger.setLevel(logging.DEBUG)
    input_cdo_df = cdo.get_df()
    df = join()
    logger.info(tabulate(df.tail(100), headers='keys', tablefmt='psql'))