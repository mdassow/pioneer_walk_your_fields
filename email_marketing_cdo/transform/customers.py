import pandas as pd
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')

from email_marketing_cdo.extract import wyf_seeds, terr_seg, cdo as cdo_extracter, cdo, segment_sender, \
    previous_wyf_seeds_int_ext


def join() -> pd.DataFrame:
    """Requests cdo extract, makes transformations, processes filters and returns data as pandas data frame"""
    input_cdo_df = cdo_extracter.get_df()
    join_df = input_cdo_df
    join_df['Territory Code'] = join_df['Territory'].str[:3]
    join_df['Territory'] = join_df['Territory'].str[:3]
    terr_seg_df = terr_seg.load_df()[['Terr_Code', 'Segment', 'is_active', 'Area']]
    terr_seg_df.rename(columns={'is_active':'terr_seg_is_active'}, inplace=True)
    join_df = join_df.merge(terr_seg_df, how='left', left_on='Territory Code', right_on='Terr_Code')
    segment_sender_df = segment_sender.load_df()
    segment_sender_df.rename(columns={'is_active':'segment_sender_is_active'}, inplace=True)
    join_df = join_df.merge(segment_sender_df, how='left', left_on='Segment', right_on='SegmentName')

    # Update AudienceID and SourceID for anyone who is in the customer list (if they are in the seed list they should be AudienceID = 3 and SourceID = 2
    seeds_df = wyf_seeds.load_df()
    join_df.loc[join_df['Email Address'].isin(seeds_df['email_address']), 'AudienceID'] = '3'
    join_df.loc[join_df['Email Address'].isin(seeds_df['email_address']), 'SourceID'] = '2'

    # Update AudienceID and SourceID for anyone who is in the previous Previous_WYF_Seeds_Int_Ext table
    previous_wyf_seeds_int_ext_df = previous_wyf_seeds_int_ext.load_df()[['EmailAddress', 'AudienceID', 'SourceID']]
    previous_wyf_seeds_int_ext_df.rename(columns={'EmailAddress':'EmailAddressTemp'
                                                    , 'AudienceID': 'AudienceIDTemp'
                                                    ,'SourceID': 'SourceIDTemp'}, inplace=True)
    join_df = join_df.merge(previous_wyf_seeds_int_ext_df, how='left', left_on='Email Address', right_on='EmailAddressTemp')
    join_df.loc[pd.isnull(join_df['AudienceIDTemp']) == False, 'AudienceID'] = join_df['AudienceIDTemp']
    join_df.loc[pd.isnull(join_df['SourceIDTemp']) == False, 'SourceID'] = join_df['SourceIDTemp']
    join_df.drop(['EmailAddressTemp', 'AudienceIDTemp','SourceIDTemp'], axis=1, inplace=True)

    join_df = process_filters(join_df=join_df)
    return join_df

def process_filters(join_df):
    join_df['filter_reason'] = ''

    # check_terr_seg_status
    join_df.loc[join_df['terr_seg_is_active'] == False, 'passed_terr_seg_status'] = False
    join_df.loc[join_df['terr_seg_is_active'] == False, 'filter_reason'] = join_df['filter_reason'] + 'Terr_Seg was not active,'
    join_df.loc[pd.isnull(join_df['terr_seg_is_active']), 'passed_terr_seg_status'] = False
    join_df.loc[pd.isnull(join_df['terr_seg_is_active']), 'filter_reason'] = join_df['filter_reason'] + 'Terr_Seg was NULL,'
    join_df.loc[join_df['terr_seg_is_active'] == True, 'passed_terr_seg_status'] = True

    # check_segment_sender_status
    join_df.loc[join_df['segment_sender_is_active'] == False, 'passed_segment_sender_status'] = False
    join_df.loc[join_df['segment_sender_is_active'] == False, 'filter_reason'] = join_df['filter_reason'] + 'Segment_Sender was not active,'
    join_df.loc[pd.isnull(join_df['segment_sender_is_active']), 'passed_segment_sender_status'] = False
    join_df.loc[pd.isnull(join_df['segment_sender_is_active']), 'filter_reason'] = join_df['filter_reason'] + 'Segment_Sender was NULL,'
    join_df.loc[join_df['segment_sender_is_active'] == True, 'passed_segment_sender_status'] = True

    return join_df

if __name__=='__main__':
    from tabulate import tabulate
    logger.setLevel(logging.DEBUG)
    input_cdo_df = cdo.get_df()
    df = join()
    logger.info(tabulate(df.tail(100), headers='keys', tablefmt='psql'))