import access_connector
import pandas as pd
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')

def filter(df):
    filter_report = dict()
    filter_report['before_filters'] = {'count_total_records_failed': 0
                                                         , 'count_result_records_newly_failed' : 0
                                                         , 'count_total_records_remaining_after_filter': len(df)
                                                         }

    newly_failed_count = len(df[df['passed_terr_seg_status'] == 0])
    results_df = df[df['passed_terr_seg_status'] == 1]
    filter_report['passed_terr_seg_status'] = {'count_total_records_failed': len(df[df['passed_terr_seg_status'] == 0])
                                                         , 'count_result_records_newly_failed' : newly_failed_count
                                                            , 'count_total_records_remaining_after_filter': len(results_df)
                                                         }

    newly_failed_count = len(results_df[results_df['passed_segment_sender_status'] == 0])
    results_df = results_df[results_df['passed_segment_sender_status'] == 1]
    filter_report['passed_segment_sender_status'] = {'count_total_records_failed': len(df[df['passed_segment_sender_status'] == 0])
                                                         , 'count_result_records_newly_failed' : newly_failed_count
                                                            , 'count_total_records_remaining_after_filter': len(results_df)
                                                         }
    return results_df, filter_report
