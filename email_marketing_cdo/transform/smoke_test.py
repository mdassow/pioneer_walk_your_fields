from os import sys, path
sys.path.append(path.dirname(path.abspath(__file__)))
import pandas as pd
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')

from email_marketing_cdo.extract import smoke_test


def join() -> pd.DataFrame:
    """Requests smoke test extract, makes transformations, processes filters and returns data as pandas data frame"""
    smoke_test_df = smoke_test.load_df()
    # Override AudienceID and SourceID to be the seed values
    smoke_test_df['AudienceID'] = '3'
    smoke_test_df['SourceID'] = '2'
    join_df = smoke_test_df
    join_df = process_filters(join_df=join_df)
    return join_df

def process_filters(join_df):
    join_df['filter_reason'] = ''

    # check_terr_seg_status
    join_df.loc[join_df['terr_seg_is_active'] == False, 'passed_terr_seg_status'] = False
    join_df.loc[join_df['terr_seg_is_active'] == False, 'filter_reason'] = join_df['filter_reason'] + 'Terr_Seg was not active,'
    join_df.loc[pd.isnull(join_df['terr_seg_is_active']), 'passed_terr_seg_status'] = False
    join_df.loc[pd.isnull(join_df['terr_seg_is_active']), 'filter_reason'] = join_df['filter_reason'] + 'Terr_Seg was NULL,'
    join_df.loc[join_df['terr_seg_is_active'] == True, 'passed_terr_seg_status'] = True

    # check_segement_sender_status
    join_df.loc[join_df['segment_sender_is_active'] == False, 'passed_segment_sender_status'] = False
    join_df.loc[join_df['segment_sender_is_active'] == False, 'filter_reason'] = join_df['filter_reason'] + 'Segment_Sender was not active,'
    join_df.loc[pd.isnull(join_df['segment_sender_is_active']), 'passed_segment_sender_status'] = False
    join_df.loc[pd.isnull(join_df['segment_sender_is_active']), 'filter_reason'] = join_df['filter_reason'] + 'Segment_Sender was NULL,'
    join_df.loc[join_df['segment_sender_is_active'] == True, 'passed_segment_sender_status'] = True

    return join_df


if __name__=="__main__":
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
    df = join()
    logging.debug(df.head())