from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import copy
import pandas as pd
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')

from email_marketing_cdo.extract import wyf_seeds, terr_seg, segment_sender


def join(input_cdo_df):
    try:
        """Requests seeds extract, joins with input cdo df, makes transformations, processes filters and returns data as pandas data frame"""
        # Get the seed emails that aren't already in the cdo_df
        seeds_df = wyf_seeds.load_df()[['email_address', 'message_id']].drop_duplicates()
        input_cdo_df['in_input_cdo_df'] = True

        # Build up records for seeds
        segment_sender_df = segment_sender.load_df()

        # Override AudienceID and SourceID to be the seed values
        segment_sender_df['AudienceID'] = '3'
        segment_sender_df['SourceID'] = '2'
        segment_sender_df.rename(columns={'is_active':'segment_sender_is_active'}, inplace=True)
        terr_seg_df = terr_seg.load_df()[['Terr_Code', 'Segment', 'is_active', 'Area']]
        terr_seg_df = terr_seg_df.groupby(['Segment', 'is_active', 'Area']) \
            .agg('min').reset_index(name='Terr_Code').drop('level_3', axis=1)
        # Having issues getting the District at the same time as the Terr_Code, so hacking in by getting it separately then merging dfs back together
        terr_seg_df_district = terr_seg.load_df()[['District', 'Segment', 'is_active', 'Area']]
        terr_seg_df_district = terr_seg_df_district.groupby(['Segment', 'is_active', 'Area']) \
            .agg('min').reset_index(name='District').drop('level_3', axis=1)
        terr_seg_df = terr_seg_df.merge(terr_seg_df_district, how='left', on=['Segment', 'is_active', 'Area'])
        terr_seg_df.rename(columns={'is_active':'terr_seg_is_active', 'Terr_Code':'Territory'}, inplace=True)
        combined_df = segment_sender_df.merge(terr_seg_df, how='left', left_on=['SegmentName'], right_on=['Segment']).drop('Segment', axis=1)
        combined_df['Territories'] = combined_df['Territory']

        # If an email is already in the input_cdo_df then I don't want to add another Seed email. This would cause a duplicate
        missing_seeds = seeds_df.merge(input_cdo_df, how='left', left_on=['email_address'], right_on=['EmailAddress']).drop(['MessageID', 'EmailAddress'], axis=1)
        missing_seeds = missing_seeds.rename(columns={'email_address':'EmailAddress'})
        missing_seeds = missing_seeds[pd.isnull(missing_seeds['in_input_cdo_df'])][['message_id', 'EmailAddress', 'CU']]
        missing_seeds = missing_seeds.merge(combined_df, how='left', left_on='message_id', right_on='MessageID').drop('message_id', axis=1)
        missing_seeds.rename(columns={'agronomist_count':'AgronomistCount'}, inplace=True)


        # If an email is already in the input_cdo_df then I want to update the MessageID to what is held in the seeds table
        already_present_seeds = seeds_df.merge(input_cdo_df, how='left', left_on=['email_address'], right_on=['EmailAddress']).drop(['MessageID', 'EmailAddress'], axis=1)
        already_present_seeds = already_present_seeds.rename(columns={'email_address':'EmailAddress'})
        already_present_seeds = already_present_seeds[pd.isnull(already_present_seeds['in_input_cdo_df']) == False][['message_id', 'EmailAddress', 'CU']]
        # update fields of emails that are in the seeds list
        already_present_seeds = copy.deepcopy(already_present_seeds.merge(combined_df, how='left', left_on='message_id', right_on='MessageID').drop('message_id', axis=1))
        already_present_seeds.rename(columns={'agronomist_count':'AgronomistCount'}, inplace=True)
        input_cdo_df[['segment_sender_is_active', 'SegmentName', 'terr_seg_is_active']] = pd.DataFrame([[None, None, None]])
        # update_cols = ['AudienceID', 'SourceID', 'segment_sender_is_active', 'Territory', 'terr_seg_is_active', 'Area', 'District', 'SegmentName', 'Territories', 'EmailAddress', 'CU']
        update_cols = already_present_seeds.columns.tolist()
        update_cols.remove('EmailAddress')
        already_present_emails = already_present_seeds['EmailAddress']
        input_cdo_df = input_cdo_df[input_cdo_df['EmailAddress'].isin(already_present_emails) == False]
        # for email in already_present_emails:
        #     for col in update_cols:
        #         input_cdo_df.loc[input_cdo_df['EmailAddress'] == email, col] = already_present_seeds[already_present_seeds['EmailAddress'] == email][col][0]

        # missing_seeds = missing_seeds.merge(terr_seg_df, how='left', left_on=['SegmentName'], right_on=['Segment']).drop('Segment', axis=1)
        seeds_final = pd.concat([missing_seeds,already_present_seeds]).reset_index()
        seeds_final = process_filters(join_df=seeds_final)
        return seeds_final, input_cdo_df
    except Exception as e:
        raise e

def process_filters(join_df):
    join_df['filter_reason'] = ''

    # check_terr_seg_status
    join_df.loc[join_df['terr_seg_is_active'] == False, 'passed_terr_seg_status'] = False
    join_df.loc[join_df['terr_seg_is_active'] == False, 'filter_reason'] = join_df['filter_reason'] + 'Terr_Seg was not active,'
    join_df.loc[pd.isnull(join_df['terr_seg_is_active']), 'passed_terr_seg_status'] = False
    join_df.loc[pd.isnull(join_df['terr_seg_is_active']), 'filter_reason'] = join_df['filter_reason'] + 'Terr_Seg was NULL,'
    join_df.loc[join_df['terr_seg_is_active'] == True, 'passed_terr_seg_status'] = True

    # check_segement_sender_status
    join_df.loc[join_df['segment_sender_is_active'] == False, 'passed_segment_sender_status'] = False
    join_df.loc[join_df['segment_sender_is_active'] == False, 'filter_reason'] = join_df['filter_reason'] + 'Segment_Sender was not active,'
    join_df.loc[pd.isnull(join_df['segment_sender_is_active']), 'passed_segment_sender_status'] = False
    join_df.loc[pd.isnull(join_df['segment_sender_is_active']), 'filter_reason'] = join_df['filter_reason'] + 'Segment_Sender was NULL,'
    join_df.loc[join_df['segment_sender_is_active'] == True, 'passed_segment_sender_status'] = True

    return join_df

if __name__=='__main__':
    import os
    from email_marketing_cdo.config import config
    # res = join()
    # root_dir = config[os.environ.get('python_env')]['cdo_import_dir']
    # fp = os.path.join(root_dir, 'customer_df_pickle')
    # customer_df = pd.read_pickle(fp)
    # df = join(input_cdo_df=customer_df)
    # logging.debug(df.head())