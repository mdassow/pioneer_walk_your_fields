import access_connector
import sql_server_connector
import pandas as pd
from tabulate import tabulate
import logging

def compare():
    access_df = access_connector.query(sql="SELECT * FROM Eloqua_contacts")[['ContactID', 'Email Address']]\
                    .astype({'ContactID':int, 'Email Address':str})
    sql_server_df = sql_server_connector.query(sql="SELECT * FROM Eloqua_Contacts")[['ContactID', 'Email Address']]\
                    .astype({'ContactID':int, 'Email Address':str})

    joined_df = access_df.merge(sql_server_df, on='ContactID', how='inner')
    logging.debug(tabulate(joined_df[joined_df['Email Address_x'].str.lower().str.strip() != joined_df['Email Address_y'].str.lower().str.strip()].head(200), headers='keys', tablefmt='psql'))

if __name__=="__main__":
    compare()