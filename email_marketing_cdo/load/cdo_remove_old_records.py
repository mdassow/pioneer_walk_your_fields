from os import sys, path
import os
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import pandas as pd
from email_marketing_cdo.config import config
from api.eloqua_api.custom_object_bulk_data import cdo_export
from ratelimit import sleep_and_retry, limits
import logging
from tqdm import tqdm
logger = logging.getLogger('Pioneer_Eloqua_API_App')

from api.eloqua_api.custom_object_data_management import cdo_record_management

def get_from_api():
    try:
        import_cdo_custom_object_id = config[os.environ.get('python_env')]['import_cdo_custom_object_id']
        import_cdo_export_def_id = config[os.environ.get('python_env')]['import_cdo_export_def_id']
        df = cdo_export.export_as_df(custom_object_id=import_cdo_custom_object_id, export_def_id=import_cdo_export_def_id)
        assert len(df) > 0
        return df
    except Exception as e:
        logger.error(e)
        raise e

def compare(previous_load_data: pd.DataFrame, current_load_data: pd.DataFrame):
    # Find email addresses that are no longer in data to be loaded
    previous_load_data['source'] = 'previous_load'
    current_load_data['source'] = 'current_load'
    missing = previous_load_data.merge(current_load_data
                                              , how='left'
                                              , on=['EmailAddress']
                                              , suffixes=('_previous', '_current'))
    missing_ids = missing[pd.isnull(missing['source_current'])]['ID']
    missing_emails = missing[pd.isnull(missing['source_current'])]['EmailAddress']
    return missing_ids, missing_emails

@sleep_and_retry
@limits(calls=100, period=3)
def delete(custom_object_id, id):
    cdo_record_management.delete_custom_object_record_data(custom_object_id=custom_object_id, id=id)

def handler(current_load_data: pd.DataFrame):
    try:
        # Pull Down DUPPEB.18015_WalkingYourFields_CDO
        logger.info('cdo_remove_old_records starting')
        previous_load_data = get_from_api()

        # Compare DUPPEB.18015_WalkingYourFields_CDO to data being loaded
        missing_records, missing_emails = compare(previous_load_data=previous_load_data, current_load_data=current_load_data)
        import_cdo_custom_object_id = config[os.environ.get('python_env')]['import_cdo_custom_object_id']
        if len(missing_records) > 0:
            logger.debug('Removing records pertaining to emails')
            for id in tqdm(missing_records[:2000]): # Limiting max deletes to ensure we don't burn up API calls (Not sure what limit is)
                    delete(custom_object_id=import_cdo_custom_object_id, id=id)
        logger.info('cdo_remove_old_records complete')
        return missing_records
    except Exception as e:
        logger.error(e)
        raise e

if __name__=='__main__':
    logger.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    logger.addHandler(ch)
    # root_dir = config[os.environ.get('python_env')]['cdo_import_dir']
    # fp = os.path.join(root_dir, 'customer_df_pickle')
    # df = pd.read_pickle(fp)
    # deleted_email_records = handler(current_load_data=df)