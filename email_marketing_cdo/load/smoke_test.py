from datetime import datetime
import os
import pandas as pd

from email_marketing_cdo.config import config


def generate_report(df: pd.DataFrame, dest_dir: str =config[os.environ.get('python_env')]['smoke_report_export_dir']):
    """Generates smoke test report and saves to directory"""
    f_name = 'SmokeTestReport_%s%s%s.xlsx' %(f'{datetime.today().year:04}', f'{datetime.today().month:02}', f'{datetime.today().day:02}')
    fp = os.path.join(dest_dir, f_name)
    df.to_excel(fp, index=False)