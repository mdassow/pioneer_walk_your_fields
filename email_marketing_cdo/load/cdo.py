import pandas as pd
# import datetime
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')
from datetime import datetime

from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from api.eloqua_api.custom_object_bulk_data import cdo_import
from email_marketing_cdo.config import config
import os

def write_to_excel(df, **kwargs):
    try:
        logger.info('%s write_to_excel has started' %(__name__))
        f_name = 'WYF_CDO_Load_%s%s%s.xlsx' %(f'{datetime.today().month:02}', f'{datetime.today().day:02}', f'{datetime.today().year:04}')
        if 'before_filters' in kwargs.keys():
            if kwargs['before_filters']:
                fp = os.path.join(config[os.environ.get('python_env')]['cdo_export_dir'], 'output_before_filters', f_name)
            else:
                fp = os.path.join(config[os.environ.get('python_env')]['cdo_export_dir'], f_name)
        else:
            fp = os.path.join(config[os.environ.get('python_env')]['cdo_export_dir'], f_name)
        writer = pd.ExcelWriter(fp,
                                engine='xlsxwriter',
                                options={'strings_to_urls': False}) # using ExcelWriter to be able to ignore urls and treat as strings
        df.to_excel(writer, sheet_name='Output')
        writer.save()
        logger.info('%s write_to_excel has finished' %(__name__))
    except Exception as e:
        logger.error('%s write_to_excel has FAILED with error %s' %(__name__, e))

def write_to_csv(df, **kwargs):
    try:
        logger.info('%s write_to_csv has started' %(__name__))
        f_name = 'WYF_CDO_Load_%s%s%s.csv' %(f'{datetime.today().month:02}', f'{datetime.today().day:02}', f'{datetime.today().year:04}')
        if 'before_filters' in kwargs.keys():
            if kwargs['before_filters']:
                fp = os.path.join(config[os.environ.get('python_env')]['cdo_export_dir'], 'output_before_filters', f_name)
            else:
                fp = os.path.join(config[os.environ.get('python_env')]['cdo_export_dir'], f_name)
        else:
            fp = os.path.join(config[os.environ.get('python_env')]['cdo_export_dir'], f_name)
        df.to_csv(fp, index=False)
        logger.info('%s write_to_csv has finished' %(__name__))
    except Exception as e:
        logger.error('%s write_to_csv has FAILED with error %s' %(__name__, e))

def write_to_many(df, **kwargs):
    method_list = [
        # write_to_excel,
        # write_to_csv,
        write_to_api]
    for method in method_list:
        if 'output_to' in kwargs.keys():
            if method.__name__ in kwargs['output_to']:
                method(df, **kwargs)
        else:
            method(df, **kwargs)


def write_to_api(df, **kwargs):
    """Gets cdo params from config and passes to load handler"""
    try:
        logger.info('%s write_to_api has started' %(__name__))
        import_cdo_custom_object_id = config[os.environ.get('python_env')]['import_cdo_custom_object_id']
        import_cdo_import_def_id = config[os.environ.get('python_env')]['import_cdo_import_def_id']
        cdo_import.import_handler(df, custom_object_id=import_cdo_custom_object_id, import_def_id=import_cdo_import_def_id)
        logger.info('%s write_to_api has finished' %(__name__))
    except Exception as e:
        logger.error('%s write_to_api has FAILED with error %s' %(__name__, e))
        raise(e)

def write_df(df, **kwargs):
    logger.info('%s write_df has started' %(__name__))
    return load_method(df=df, **kwargs)

load_method = write_to_many

if __name__=='__main__':
    logger.setLevel(level=logging.DEBUG)

    # create console handler with a different log level
    ch = logging.StreamHandler()
    # ch.setFormatter(fmt=logging.Formatter.formatStack)
    ch.setLevel(logging.DEBUG)
    logger.addHandler(ch)
    # root_dir = config[os.environ.get('python_env')]['cdo_import_dir']
    # fp = os.path.join(root_dir, 'customer_df_pickle')
    # df = pd.read_pickle(fp)
    # write_df(df=df)
    # logger.info(df.head())
    # logger.info(df.columns)