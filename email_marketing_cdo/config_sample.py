config = {
             'prod': {
                'cdo_export_dir': r'K:\PIONEER\Data Management\MP CDO Uploads'
                , 'smoke_report_export_dir': r'K:\PIONEER\Data Management\Reports\Smoke Tests'
                , 'access_db_file_path' : r'K:\PIONEER\Data Management\Production\Pioneer WYF.accdb'
                , 'env': 'prod'
                , 'sql_server_user': '<USERNAME>'
                , 'sql_server_pw' : '<PASSWORD>'
                , 'mongo_cluster_url': '<mongo_cluster_url>'
                , 'mongo_server_user': '<mongo_server_user>'
                , 'mongo_server_pw': '<mongo_server_pw>'
                , 'mongo_env': '<mongo_env>'
                , 'export_cdo_custom_object_id': <export_cdo_custom_object_id>
                , 'export_cdo_export_def_id': <export_cdo_export_def_id>
                , 'import_cdo_custom_object_id': <import_cdo_custom_object_id>
                , 'import_cdo_import_def_id': <import_cdo_import_def_id>
                , 'smtp_server_config': {
                    'email_host': '<email_host>'
                    , 'email_host_port': '<email_host_port>'
                    , 'smtp_user': '<smtp_user>'
                    , 'smtp_password': '<smtp_password>'
                }
                , 'log_email_config': {
                    'from_address': '<from_address>'
                    , 'to_address_list': ['<to_address_list>']
                    , 'email_subject': '<email_subject>'
                }
                , 'summary_report_email_config': {
                    'from_address': '<summary_report_email_config>'
                    , 'to_address_list': ['<to_address>', '<to_address>']
                    , 'email_subject': '<email_subject>'
                }
            }
    ,'local': {
                'cdo_export_dir': r'C:\Users\mdassow\Documents\Testing\Pioneer\Data Management\MP CDO Uploads'
                , 'smoke_report_export_dir': r'C:\Users\mdassow\Documents\Testing\Pioneer\Data Management\Reports\Smoke Tests'
                , 'access_db_file_path' : r'C:\Users\mdassow\Documents\Testing\Pioneer\Data Management\Production\Pioneer WYF.accdb'
                , 'env': 'local'
                , 'sql_server_user': '<USERNAME>'
                , 'sql_server_pw' : '<PASSWORD>'
                , 'mongo_cluster_url': '<mongo_cluster_url>'
                , 'mongo_server_user': '<mongo_server_user>'
                , 'mongo_server_pw': '<mongo_server_pw>'
                , 'mongo_env': '<mongo_env>'
                , 'export_cdo_custom_object_id': <export_cdo_custom_object_id>
                , 'export_cdo_export_def_id': <export_cdo_export_def_id>
                , 'import_cdo_custom_object_id': <import_cdo_custom_object_id>
                , 'import_cdo_import_def_id': <import_cdo_import_def_id>
                , 'smtp_server_config': {
                    'email_host': '<email_host>'
                    , 'email_host_port': '<email_host_port>'
                    , 'smtp_user': '<smtp_user>'
                    , 'smtp_password': '<smtp_password>'
                }
                , 'log_email_config': {
                    'from_address': '<from_address>'
                    , 'to_address_list': ['<to_address_list>']
                    , 'email_subject': '<email_subject>'
                }
                , 'summary_report_email_config': {
                    'from_address': '<summary_report_email_config>'
                    , 'to_address_list': ['<to_address>', '<to_address>']
                    , 'email_subject': '<email_subject>'
                }
    }
}