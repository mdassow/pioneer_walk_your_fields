from os import sys, path

sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import pandas as pd
from datetime import datetime, timedelta
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')

import sql_server_connector


def handler(df, previous_run_stats_run_date=None) -> pd.DataFrame:
    """Compare cdo segment counts to previous run. Generates a report summary data frame"""
    if previous_run_stats_run_date is None:
        previous_run_stats_run_date = get_last_load_date()
    load_stats_current = generate_load_stats(df=df)
    log_load_stats(run_stats=load_stats_current)
    summary_df = compare_load_stats(run_stats_run_date_old=previous_run_stats_run_date, run_stats_run_date_new=datetime.date(datetime.now()))
    # summary_df.to_csv('WYF_Segment_Summary%s_vs_%s.csv' %(str(previous_run_stats_run_date).replace('-','.'), str(datetime.date(datetime.now())).replace('-','.')), index=False)
    return summary_df


def has_required_fields(df):
    def check_field(field):
        if field not in df.columns:
            raise(Exception('%s field is not present in data frame'%(field)))

    required_field_list = ['MessageID', 'AudienceID']
    for f in required_field_list:
        check_field(f)


def get_last_load_date():
    last = query_run_date()
    if last == datetime.date(datetime.now()):
        max_date = datetime.date(datetime.now()) - timedelta(1)
        return query_run_date(date=max_date)
    else:
        return last


def query_run_date(date=None):
    sql_server_connector_obj = sql_server_connector.handler()
    if date is None:
        sql = """SELECT MAX(run_date) as Max_Run_Date
                FROM Summary_Stats_Log"""
    else:
        sql = """SELECT MAX(run_date) as Max_Run_Date
                FROM Summary_Stats_Log
                WHERE run_date <= '%s'"""%(date)
    res = sql_server_connector_obj.load_df(sql=sql)['Max_Run_Date'][0]
    return res


def get_previous_load_stats(date):
    sql_server_connector_obj = sql_server_connector.handler()
    sql = """SELECT run_date, message_id as MessageID, grower_count AS Grower_Count, internal_count AS Internal_Count, seed_count AS Seed_Count
                FROM Summary_Stats_Log
                WHERE run_date = '%s'"""%(date)
    res = sql_server_connector_obj.load_df(sql=sql)
    assert len(res) > 0
    return res


def log_load_stats(run_stats):
    sql_server_connector_obj = sql_server_connector.handler()
    for k, stat in run_stats.iterrows():
        sql = """IF EXISTS(select * from Summary_Stats_Log where run_date = '%s' AND message_id=%s)
                UPDATE Summary_Stats_Log 
                    SET grower_count = %s
                    , internal_count = %s
                    , seed_count = %s
                    WHERE run_date = '%s' AND message_id=%s     
                ELSE
                INSERT INTO Summary_Stats_Log
                (run_date, message_id, grower_count, internal_count, seed_count)
                VALUES('%s', %s, %s, %s, %s);
                """%(stat.run_date
                        , stat.MessageID
                        , stat.Grower_Count
                        , stat.Internal_Count
                        , stat.Seed_Count
                        , stat.run_date
                        , stat.MessageID

                        , stat.run_date
                        , stat.MessageID
                        , stat.Grower_Count
                        , stat.Internal_Count
                        , stat.Seed_Count
                     )
        sql_server_connector_obj.insert(sql=sql)


def generate_load_stats(df):
    try:
        has_required_fields(df)
        grower_count_df = df[df['AudienceID'].astype(float).astype(int) == 1].groupby(['MessageID']).agg('count')[['AudienceID']]
        grower_count_df.rename(columns={'AudienceID': 'Grower_Count'}, inplace=True)
        internal_count_df = df[df['AudienceID'].astype(float).astype(int) == 2].groupby(['MessageID']).agg('count')[['AudienceID']]
        internal_count_df.rename(columns={'AudienceID': 'Internal_Count'}, inplace=True)
        seeds_count_df = df[df['AudienceID'].astype(float).astype(int) == 3].groupby(['MessageID']).agg('count')[['AudienceID']]
        seeds_count_df.rename(columns={'AudienceID': 'Seed_Count'}, inplace=True)

        load_stats_df = df[['MessageID']].drop_duplicates()
        load_stats_df['run_date'] = datetime.date(datetime.now())
        load_stats_df = load_stats_df.merge(grower_count_df, left_on='MessageID', right_index=True)
        load_stats_df = load_stats_df.merge(internal_count_df, left_on='MessageID', right_index=True)
        load_stats_df = load_stats_df.merge(seeds_count_df, left_on='MessageID', right_index=True)

        load_stats_df.sort_values('MessageID', ascending=True, inplace=True)
        assert len(load_stats_df) > 0
        return load_stats_df
    except Exception as e:
        logger.error(e)


def compare_load_stats(run_stats_run_date_old, run_stats_run_date_new):
    load_stats_old = get_previous_load_stats(date=run_stats_run_date_old)
    load_stats_new = get_previous_load_stats(date=run_stats_run_date_new)
    summary_df = load_stats_old.merge(load_stats_new, on=['MessageID'], suffixes=('_Old', '_New'))
    summary_df['Grower_Diff'] = summary_df['Grower_Count_Old'] - summary_df['Grower_Count_New']
    summary_df['Internal_Diff'] = summary_df['Internal_Count_Old'] - summary_df['Internal_Count_New']
    summary_df['Seed_Diff'] = summary_df['Seed_Count_Old'] - summary_df['Seed_Count_New']

    summary_df['Grower_Percent_Change'] = (summary_df['Grower_Count_New'] - summary_df['Grower_Count_Old']) / summary_df['Grower_Count_Old']
    summary_df['Internal_Percent_Change'] = (summary_df['Internal_Count_New'] - summary_df['Internal_Count_Old']) / summary_df['Internal_Count_Old']
    summary_df['Seed_Percent_Change'] = (summary_df['Seed_Count_New'] - summary_df['Seed_Count_Old']) / summary_df['Seed_Count_Old']
    assert len(summary_df) > 0
    return summary_df


def validate_summary(summary_df) -> BaseException:
    """Takes in segement count summary data frame.
    Validates the counts are within count thresholds compared to previous run"""
    min_count_summary_df = summary_df
    if (((min_count_summary_df[['Grower_Percent_Change']] >= .25) == True).any() == True).any():
        return Exception('Error - WYF Summary Counts Changed more than 25%. Not updating Output CDO')
    elif (((min_count_summary_df[['Grower_Percent_Change']] >= .05) == True).any() == True).any():
        return Warning('Warning - WYF Summary Counts Changed more than 5%')
    # elif (((min_count_summary_df[['Grower_Percent_Change']] == .00) == True).all() == True).any():
    #     return Exception('Error - WYF Summary Counts No Segments Changed Counts. Not updating Output CDO')

if __name__=='__main__':
    import os
    from email_marketing_cdo.config import config
    from tabulate import tabulate
    root_dir = config[os.environ.get('python_env')]['cdo_import_dir']
    fp = os.path.join(root_dir, 'final_cdo_pickle')
    df = pd.read_pickle(fp)

    summary_df = handler(df=df)
    print(tabulate(summary_df, headers='keys', tablefmt='psql'))