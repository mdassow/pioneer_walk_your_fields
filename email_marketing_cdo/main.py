from datetime import datetime
import os
from os import sys, path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
import logging
from log_handling import run_log_handler, email_log_handler
from tabulate import tabulate

logger = logging.getLogger('Pioneer_Eloqua_API_App')
logger.setLevel(logging.DEBUG)
from models.run_log import run_log, qa_report
from email_marketing_cdo.config import config

# Create MongoDB run log logging handler
r_log = run_log(account='marketing_cdo'
                , channel='eloqua'
                , client_id=29
                , client_name='Pioneer'
                )
r_log.save()
handler = run_log_handler.run_log_handler(run_log_obj=r_log)
handler.setLevel(logging.INFO)
logger.addHandler(handler)

# Set up email log handler
email_handler = email_log_handler.BufferingSMTPHandler(mailhost=config[os.environ.get('python_env')]['smtp_server_config']['email_host']
                                                       , mailport=config[os.environ.get('python_env')]['smtp_server_config']['email_host_port']
                                                       , fromaddr=config[os.environ.get('python_env')]['log_email_config']['from_address']
                                                       , toaddrs=config[os.environ.get('python_env')]['log_email_config']['to_address_list']
                                                       , subject=config[os.environ.get('python_env')]['log_email_config']['email_subject']
                                                       , smtp_user=config[os.environ.get('python_env')]['smtp_server_config']['smtp_user']
                                                       , smtp_password=config[os.environ.get('python_env')]['smtp_server_config']['smtp_password'])
email_handler.setLevel(logging.WARNING)
logger.addHandler(email_handler)

# create console handler with a different log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
logger.addHandler(ch)

from email_marketing_cdo.transform import customers, smoke_test as smoke_test_transformer, filters, seeds, previous_wyf_seeds
from email_marketing_cdo.load import smoke_test as smoke_test_loader, cdo as cdo_loader, cdo_remove_old_records
from email_marketing_cdo.reporting import segment_review_handler, emailer
from email_marketing_cdo.migrate_access_tables_to_sql_server.python_data_migrations import run_upload_support_tables
from api.eloqua_api.auth import refresh_token

def run() -> None:
    """Orchestrates the ETL process for Pioneer Eloqua Marketing CDO extract process"""
    try:
        # refresh_token.handle_auth_token_refresh()
        logger.info('run process has started for %s' %(os.environ.get('python_env')))
        run_upload_support_tables.run()
        logger.info('input_cdo_df created')


        # Load input cdo and pass to customer and seed generator
        output_col_list = 'EmailAddress	CU	Area	District	Territory	Territories	AgronomistName1	AgronomistTitle1	AgronomistWorkEmail1	AgronomistPhoneNumber1	AgronomistTwitterHandle1	AgronomistHeadshot1	AgronomistHeadshotFilePath1	AgronomistName2	AgronomistTitle2	AgronomistWorkEmail2	AgronomistPhoneNumber2	AgronomistTwitterHandle2	AgronomistHeadshot2	AgronomistHeadshotFilePath2	AgronomistName3	AgronomistTitle3	AgronomistWorkEmail3	AgronomistPhoneNumber3	AgronomistTwitterHandle3	AgronomistHeadshot3	AgronomistHeadshotFilePath3	AgronomistName4	AgronomistTitle4	AgronomistWorkEmail4	AgronomistPhoneNumber4	AgronomistTwitterHandle4	AgronomistHeadshot4	AgronomistHeadshotFilePath4	AgronomistCount	CC	DataSource	AudienceID	MessageID	SourceID	Channel	VersionName	CreativeName	CreativeID	CampaignID	CID'.split('\t')
        support_col_list = {'passed_terr_seg_status', 'passed_segment_sender_status'}
        col_list = set(output_col_list).union(support_col_list)

        customer_df = customers.join()
        logger.debug('customer_df size: %f MB' %(customer_df.memory_usage().sum()/1024/1024))
        customer_df.rename(columns={'Territory Code':'Territories'
                                        , 'Email Address':'EmailAddress'
                                        , 'Commercial_Unit':'CU'
                                        , 'agronomist_count':'AgronomistCount'}, inplace=True)
        customer_df = customer_df[col_list]
        customer_df['record_type'] = 'customer'
        # pickle the results to be able to pick up later in testing if running locally.
        # if os.environ.get('python_env') == 'local':
        #     root_dir = config[os.environ.get('python_env')]['cdo_import_dir']
        #     fp = os.path.join(root_dir, 'customer_df_pickle')
        #     customer_df.to_pickle(fp)
        logger.info('customer_df created')

        seeds_df, customer_df = seeds.join(input_cdo_df=customer_df) # reset customer_df to exclude emails that were removed during seed emails transform
        logger.debug('seeds_df size: %f MB' %(seeds_df.memory_usage().sum()/1024/1024))
        seeds_df.rename(columns={'Territory Code':'Territories'
                                        , 'Email Address':'EmailAddress'
                                        , 'Commercial_Unit':'CU'}, inplace=True)

        seeds_df = seeds_df[col_list]
        seeds_df.loc[:, 'record_type'] = 'seed'
        logger.info('seeds_df created')

        smoke_test_df = smoke_test_transformer.join()
        smoke_test_df.rename(columns={'Territory Code':'Territories'
                                        , 'Email Address':'EmailAddress'
                                        , 'Commercial_Unit':'CU'
                                        , 'agronomist_count':'AgronomistCount'}, inplace=True)
        smoke_test_df = smoke_test_df[col_list]
        logger.debug('smoke_test_df size: %f MB' %(smoke_test_df.memory_usage().sum()/1024/1024))
        smoke_test_df['record_type'] = 'smoke_test'
        logger.info('smoke_test_df created')


        # Combine customers and seeds into one df
        combined_df = customer_df.append([seeds_df, smoke_test_df], sort=False).reindex()
        logger.debug('combined_df size: %f MB' %(combined_df.memory_usage().sum()/1024/1024))


        # add placeholder field 'Area' to df to support legacy format
        combined_df['Area'] = None

        # Rename columns to match previous output
        combined_df.rename(columns={'Territory Code':'Territories'
                                        , 'Email Address':'EmailAddress'
                                        , 'Commercial_Unit':'CU'
                                        , 'agronomist_count':'AgronomistCount'}, inplace=True)
        logger.info('combined_df created')

        combined_df = previous_wyf_seeds.join(combined_df)
        logger.info('previous_wyf_seeds created')

        # Call out missing columns:
        missing_cols = col_list.difference(set(combined_df.columns))
        if len(missing_cols) > 0:
            logger.error('combined_df is missing required columns. Missing_cols: %s' % str(missing_cols))

        # Convert dtypes
        # combined_df = combined_df.astype(dtype={'Area': str, 'MessageID': str, 'CreativeID': str, 'AgronomistCount': str, 'SourceID':str, 'AudienceID': str})

        # Format float decimal places
        # for

        # Filter out records that aren't wanted in the final output
        final_df, filter_report = filters.filter(df=combined_df)

        # Remove duplicates
        final_df.drop_duplicates(inplace=True)
        logger.debug('final_df size: %f MB' %(final_df.memory_usage().sum()/1024/1024))

        # Generate Segment Summary Report and validate count changes are within tolerances
        summary_df = segment_review_handler.handler(df=final_df)
        logger.debug('summary_df size: %f MB' %(summary_df.memory_usage().sum()/1024/1024))
        validated = segment_review_handler.validate_summary(summary_df=summary_df)
        if validated is not None:
            if type(validated) is Exception:
                email_handler.subject = 'ERROR - %s'%(email_handler.subject)
                logger.log(level=100, msg='Summary results:\n\n %s'%(tabulate(summary_df, headers='keys', tablefmt='psql')))
                raise validated
            elif type(validated) is Warning:
                email_handler.subject = 'Warning - %s'%(email_handler.subject)
                logger.warning(str(validated))

        # write to file and/or API.
        cdo_loader.write_df(df=final_df[output_col_list])
        logger.info('final_df complete')

        # Remove old records from import CDO that are no longer in current CDO (to keep in sync)
        cdo_remove_old_records.handler(current_load_data=final_df)


        # Generate smoke_testing_report
        # try:
        #     smoke_test_loader.generate_report(df=smoke_test_df[output_col_list])
        # except Exception as e:
        #     logger.error(e)
        logger.info('%s process has finished' %(__name__))

        qa_rep = qa_report(input_record_count=1, output_record_count=1)
        r_log.qa_report = qa_rep
        r_log.qa_status = 1
        r_log.run_end_datetime = datetime.now()
        r_log.save()

        emailer.handler().generate_email(df=summary_df)
        logger.info('Summary results:\n\n %s'%(tabulate(summary_df, headers='keys', tablefmt='psql')))
        logging.shutdown()

    except Exception as e:
        logger.error('%s process has failed with error %s' % (__name__, e), exc_info=True)
        logging.shutdown()


if __name__=='__main__':
    import time
    def run_at_time(time_to_run, mins_between_run, last_run_time=None):
        import datetime as dt
        logger.debug('Starting run_at_time')
        if last_run_time is None:
            logger.info('No last run time provided. Running via run_at_time function')
            last_run_time = datetime.now()
            run()
        while True:
            try:
                if last_run_time + dt.timedelta(minutes=mins_between_run) < datetime.now():
                    if datetime.now() >= time_to_run:
                        logger.info('Running via run_at_time function')
                        last_run_time_start = datetime.now()
                        run()
                        last_run_time = last_run_time_start # On successful run reset last run time
                        time_to_run = datetime(year=current_time.year, month=current_time.month, day=current_time.day + 1, hour=5, minute=0, second=0)
                time.sleep(60)

            except Exception as e:
                logger.error(e, exc_info=True)


    current_time = datetime.now()
    time_to_run = datetime(year=current_time.year, month=current_time.month, day=current_time.day + 1, hour=5, minute=0, second=0)
    last_run_time = datetime(year=current_time.year, month=current_time.month, day=current_time.day, hour=5, minute=0, second=0)


    time_to_run = datetime(year=current_time.year, month=current_time.month, day=current_time.day, hour=5, minute=0, second=0)
    last_run_time = datetime(year=current_time.year, month=current_time.month, day=current_time.day - 1, hour=5, minute=0, second=0)
    run_at_time(time_to_run=time_to_run, mins_between_run=60*24, last_run_time=last_run_time)

    # run()