import re
def check_if_is_id_field(key):
    delimiters = ['_', '-']
    for delim in delimiters:
        print(key)
        key = key.replace(delim, ' ')
        print(key)
    tokenized_key = re.findall(r"[\w']+", key)
    print(tokenized_key)
    if 'id' in tokenized_key:
        return True
    return False

print(check_if_is_id_field('test_id'))