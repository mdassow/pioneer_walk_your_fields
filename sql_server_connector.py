import pyodbc

import sqlalchemy.pool as pool
import pandas as pd
import os

from email_marketing_cdo.config import config
import logging
logger = logging.getLogger('Pioneer_Eloqua_API_App')

class handler:
    def __init__(self, pool_size=1, max_overflow=10):
        self.pool = pool.QueuePool(self.connect, max_overflow=max_overflow, pool_size=pool_size)

    def connect(self):
        user = config[os.environ.get('python_env')]['sql_server_user']
        pw = config[os.environ.get('python_env')]['sql_server_pw']
        conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=18.213.233.196;DATABASE=Pioneer;UID=%s;PWD={%s};' %(user, pw))
        return conn

    def query(self, sql):
        conn = self.pool.connect()
        cursor = conn.cursor()
        cursor.execute(sql)
        columns = cursor.description
        res = [dict(zip([column[0] for column in columns], row)) for row in cursor.fetchall()]
        return res

    def insert(self, sql, try_count=1):
        try:
            conn = self.connect()
            cursor = conn.cursor()
            cursor.execute(sql)
            cursor.commit()
            conn.close()
        except Exception as e:
            logger.debug(e)
            try_count += 1
            if try_count <= 3:
                self.insert(sql=sql, try_count=try_count)
            else:
                raise e

    def load_df(self, sql):
        return pd.DataFrame(self.query(sql=sql))


if __name__=="__main__":
    from tabulate import tabulate
    h = handler()
    sql = """SELECT * FROM Previous_WYF_Seeds_Int_Ext"""
    df = h.query(sql)
    print(tabulate(df, headers='keys', tablefmt='psql'))
    pass