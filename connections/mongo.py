import pymongo
import pandas as pd
import os

from email_marketing_cdo.config import config

class handler:
    def connect(self, db=''):
        user = config[os.environ.get('python_env')]['mongo_server_user']
        pw = config[os.environ.get('python_env')]['mongo_server_pw']
        cluster_url = config[os.environ.get('python_env')]['mongo_cluster_url']
        conn_string = 'mongodb+srv://%s:%s@%s/%s?retryWrites=true&w=majority' %(user, pw, cluster_url, db)
        client = pymongo.MongoClient(conn_string)
        return client

    def query(self, database, collection, query_dict=None, projection=None):
        client = self.connect()
        db = client[database]
        coll = db[collection]
        record_list = [x for x in coll.find(query_dict, projection)]
        # pprint.pprint(record_list)
        return record_list

    def insert(self, database, collection, insert_obj_list):
        client = self.connect()
        db = client[database]
        coll = db[collection]
        coll.insert_many(insert_obj_list)

    def show_collections(self, database):
        client = self.connect()
        db = client[database]
        colls = db.collection_names()
        return colls

    def show_dbs(self):
        client = self.connect()
        return client.list_database_names()


    def load_df(self, database, collection, query_dict=None):
        res_list = self.query(database= database, collection=collection, query_dict=query_dict)
        df = pd.DataFrame(res_list)
        return df


if __name__=="__main__":
    # from tabulate import tabulate
    # df = handler().query()

    print(handler().show_dbs())
    # df = handler().load_df(database='prod', collection='elt_run_logs', query_dict={'_id': ObjectId('5cfa865ff78bbc0008886e4f')})
    # df = handler().load_df(database='prod', collection='elt_run_logs')

    # df = handler().query(database='prod', collection='elt_run_logs', query_dict={'_id':ObjectId('5cfa865ff78bbc0008886e4f')})
    # df = handler().query(database='prod', collection='elt_run_logs')
    # print(tabulate(df, headers='keys', tablefmt='psql'))
    pass